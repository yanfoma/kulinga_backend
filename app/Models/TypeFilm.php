<?php
/**
 * Created by PhpStorm.
 * User: Wendkouny
 * Date: 03/02/2019
 * Time: 10:06
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class TypeFilm extends Model
{
    public $table = 'type_films';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'libelle_type',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'libelle_type' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function films() : HasMany {
        return $this->hasMany('App\Models\Film');
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: Wendkouny
 * Date: 03/02/2019
 * Time: 10:16
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{

    public $table = 'promos';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'libelle_promo',
        'description',
        'date_debut',
        'date_fin'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'libelle_promo' => 'string',
        'description' => 'string',
        'date_debut' => 'datetime',
        'date_fin' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
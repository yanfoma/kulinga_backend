<?php
/**
 * Created by PhpStorm.
 * User: Wendkouny
 * Date: 03/02/2019
 * Time: 11:12
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PrixFilm extends Model
{
    public $table = 'prix_films';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'titre_prix',
        'description_prix',
        'valeur_prix',
        'film_id'
    ];


    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function film() : BelongsTo {
        return $this->belongsTo('App\Models\Film');
    }
}
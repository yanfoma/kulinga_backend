<?php
/**
 * Created by PhpStorm.
 * User: Wendkouny
 * Date: 03/02/2019
 * Time: 10:37
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AvisAbonne extends Model
{
    public $table = 'avis_abonnes';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'titre',
        'message',
        'user_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'titre' => 'string',
        'message' => 'string',
        'user_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'titre'=>'required',
        'message'=>'required',
        'user_id' => 'required'
    ];

    public function user() : BelongsTo {
        return $this->belongsTo('App\Models\User');
    }

}
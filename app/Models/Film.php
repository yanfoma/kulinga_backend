<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Film
 * @package App\Models\Admin
 * @version December 29, 2018, 5:11 am UTC
 *
 * @property string titre
 * @property string description
 * @property string|\Carbon\Carbon date_realisation
 * @property integer duree_film
 * @property bigInteger nb_vues
 * @property string type_film
 * @property string url_film
 * @property string pub_key_film
 * @property string url_image
 * @property string pub_key_image
 * @property integer user_id
 * @property integer auteur_id
 */
class Film extends Model
{

    public $table = 'films';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'titre',
        'description',
        'date_realisation',
        'duree_film',
        'nb_vues',
        'url_film',
        'pub_key_film',
        'url_image',
        'pub_key_image',
        'identite_distributeur',
        'adresse_distributeur',
        'contact_distributeur',
        'user_id',
        'auteur_id',
        'type_film_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'titre' => 'string',
        'description' => 'string',
        'duree_film' => 'integer',
        'url_film' => 'string',
        'pub_key_film' => 'string',
        'url_image' => 'string',
        'identite_distributeur' => 'string',
        'adresse_distributeur' => 'string',
        'contact_distributeur' => 'string',
        'pub_key_image' => 'string',
        'user_id' => 'integer',
        'auteur_id' => 'integer',
        'type_film_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'titre'=>'required',
        'type_film'=>'required',
        'url_film'=> "required|mimes:mp4|between:1, 100048",
        'url_image'=> "required|mimes:jpeg,bmp,jpg,png|between:1, 2048",
        'type_film_id' =>'required'
    ];

    public function user() : BelongsTo {
        return $this->belongsTo('App\Models\User');
    }

    public function typefilm() : BelongsTo {
        return $this->belongsTo('App\Models\TypeFilm');
    }

    public function auteur() : BelongsTo {
        return $this->belongsTo('App\Models\Auteur');
    }

    public function notations() : HasMany {
        return $this->hasMany('App\Models\Notation');
    }

    public function prixFilms() : HasMany {
        return $this->hasMany('App\Models\PrixFilm');
    }

    public function categories() : BelongsToMany {
        return $this->belongsToMany('App\Models\Categorie', 'categorie_has_films');
    }
}

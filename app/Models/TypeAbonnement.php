<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class TypeAbonnement
 * @package App\Models\Admin
 * @version December 29, 2018, 6:13 am UTC
 *
 * @property string libelle
 * @property decimal mt_mensuel
 */
class TypeAbonnement extends Model
{
    public $table = 'type_abonnements';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'libelle',
        'mt_mensuel',
        'taux_echange'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function abonnements() : HasMany {
        return $this->hasMany('App\Models\Abonnement');
    }
    
}

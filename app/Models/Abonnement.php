<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Abonnement
 * @package App\Models\Admin
 * @version December 29, 2018, 6:13 am UTC
 *
 * @property integer nb_mois
 * @property integer user_id
 * @property integer typeabonnement_id
 */
class Abonnement extends Model
{

    public $table = 'abonnements';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'nb_mois',
        'user_id',
        'typeabonnement_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nb_mois' => 'integer',
        'user_id' => 'integer',
        'typeabonnement_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function user() : BelongsTo {
        return $this->belongsTo('App\Models\User');
    }

    public function typeabonnements() : BelongsTo {
        return $this->belongsTo('App\Models\TypeAbonnement');
    }
    
}

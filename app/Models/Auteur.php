<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Auteur
 * @package App\Models\Admin
 * @version December 29, 2018, 6:12 am UTC
 *
 * @property string nom
 * @property integer pays_id
 */
class Auteur extends Model
{

    public $table = 'auteurs';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'nom',
        'pays'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nom' => 'string',
        'pays' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function films() : HasMany {
        return $this->hasMany('App\Models\Film');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Notation
 * @package App\Models\Admin
 * @version December 29, 2018, 6:20 am UTC
 *
 * @property bigInteger nb_etoile
 * @property integer film_id
 */
class Notation extends Model
{

    public $table = 'notations';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'nb_etoile',
        'film_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'film_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function film() : BelongsTo {
        return $this->belongsTo('App\Models\Film');
    }
    
}

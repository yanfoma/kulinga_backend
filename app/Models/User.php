<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class User
 * @package App\Models\Admin
 * @version December 29, 2018, 6:20 am UTC
 *
 * @property string nom
 * @property string prenom
 * @property string telephone
 * @property string login
 * @property string email
 * @property string|\Carbon\Carbon email_verified_at
 * @property string password
 * @property integer pays_id
 * @property string remember_token
 */
class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    public $table = 'users';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'nom',
        'prenom',
        'telephone',
        'login',
        'email',
        'pays',
        'password',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nom' => 'string',
        'prenom' => 'string',
        'telephone' => 'string',
        'login' => 'string',
        'email' => 'string',
        'password' => 'string',
        'pays' => 'string',
        'remember_token' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function abonnements() : HasMany{
        return $this->hasMany('App\Models\Abonnement');
    }

    public function films() : HasMany {
        return $this->hasMany('App\Models\Film');
    }

    public function avisAbonnes() : HasMany {
        return $this->hasMany('App\Models\AvisAbonne');
    }
}

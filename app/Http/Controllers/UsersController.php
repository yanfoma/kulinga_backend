<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the User.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $users = User::all();

        return view('admin.users.index')
            ->with('users', $users);
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.users.create')->with(['users' => null]);
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUsersRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nom' => ['required', 'string', 'max:255'],
            'prenom' => ['required', 'string', 'max:255'],
            'login' => ['required', 'string', 'max:255','unique:users'],
            'telephone' => ['string','unique:users', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'pays' => ['string', 'required'],
        ]);

        $input = $request->all();

        $user = User::create([
            'nom' => $input['nom'],
            'prenom' => $input['prenom'],
            'telephone' => $input['telephone'],
            'login' => $input['login'],
            'email' => $input['email'],
            'pays' => $input['pays'],
            'password' => Hash::make($input['password'])
        ]);

        $user->syncRoles(['subscriber']);

        return redirect(route('admin.users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $users = User::findOrFail($id);

        if (empty($users)) {
            return redirect(route('admin.users.index'));
        }

        return view('admin.users.show')->with('users', $users);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $users = User::findOrFail($id);

        if (empty($users)) {
            return redirect(route('admin.users.index'));
        }

        return view('admin.users.edit')->with('users', $users);
    }

    /**
     * Update the specified User in storage.
     *
     * @param  int              $id
     * @param Request $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $request->validate([
            'nom' => ['required', 'string', 'max:255'],
            'prenom' => ['required', 'string', 'max:255'],
            'login' => ['required', 'string', 'max:255','unique:users'],
            'telephone' => ['string','unique:users', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'pays' => ['string', 'required'],
        ]);

        $users = User::findOrFail($id);

        if (empty($users)) {
            return redirect(route('admin.users.index'));
        }

        $inputs = $request->all();

        $users->update($inputs);

        return redirect(route('admin.users.index'));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $users = User::findOrFail($id);

        if (empty($users)) {
            return redirect(route('admin.users.index'));
        }

        $users->delete($id);

        return redirect(route('admin.users.index'));
    }
}

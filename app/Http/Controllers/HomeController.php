<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use App\Models\Promo;
use App\Models\TypeAbonnement;
use Symfony\Component\HttpFoundation\Request;
use App\Models\Film;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   /* public function __construct()
    {
        $this->middleware('auth');
    }*/

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Categorie::get();
        //Controle d'affichage de promo d'une periode
        $promo = Promo::get()->last();
        if (isset($promo)) {
            if ($promo->date_debut <= date_create(date_default_timezone_get()) && $promo->date_fin >= date_create(date_default_timezone_get())) {
                $nbJr = date_diff($promo->date_debut, $promo->date_fin, true)->days;
                $unepromo = 1;
            } else {
                $unepromo = 0;
            }
        }
        $typeAbonnements = TypeAbonnement::all();
        return view('lists', compact('categories', 'typeAbonnements', 'promo', 'nbJr', 'unepromo'));

    }

}
<?php
/**
 * Created by PhpStorm.
 * User: Wendkouny
 * Date: 16/01/2019
 * Time: 04:56
 */

namespace App\Http\Controllers;

use App\Models\Abonnement;
use App\Models\TypeAbonnement;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class EspaceAbonneController extends Controller {

    use RegistersUsers;

    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display a listing of the Abonnement.
     *
     * @return Response
     */
    public function index()
    {
        $abonnements = DB::table('abonnements')->where('user_id', Auth::id())->orderBy('created_at', 'desc')->get();
        if(!$abonnements->isEmpty()) {
            $ab_first = $abonnements->first();
            if(date_diff(date_create(date_default_timezone_get()), date_create($ab_first->created_at))->days <= ($ab_first->nb_mois*30)-5){
                $can_subscribe = 'cannot';
            } else {
                $can_subscribe = 'can';
            }
        } else {
            $can_subscribe = 'can';
        }

        $type_abonnements = TypeAbonnement::all();
        return view('frontEnd.Abonnnements.espaceClient', compact('abonnements' , 'type_abonnements', 'can_subscribe'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => ['required', 'string', 'max:255'],
            'prenom' => ['required', 'string', 'max:255'],
            'login' => ['required', 'string', 'max:255'],
            'telephone' => ['string','unique:users', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'pays' => ['string', 'required'],
        ]);
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        return view('frontEnd.Abonnnements.createAbonnement');
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUsersRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nom' => ['required', 'string', 'max:255'],
            'prenom' => ['required', 'string', 'max:255'],
            'login' => ['required', 'string', 'max:255','unique:users'],
            'telephone' => ['string','unique:users', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'pays' => ['string', 'required'],
        ]);

        $input = $request->all();

        $user = User::create([
            'nom' => $input['nom'],
            'prenom' => $input['prenom'],
            'telephone' => $input['telephone'],
            'login' => $input['login'],
            'email' => $input['email'],
            'pays' => $input['pays'],
            'password' => Hash::make($input['password'])
        ]);

        $user->syncRoles(['subscriber']);

        Auth::login($user);

        return redirect(route('espace.abonne.home'))->with('user', $user);
    }

    public function edit(Request $request) {
        $inputs = $request->all();
        $request->validate([
            'nombre_mois' => ['integer', 'required'],
            'type_abonnement' => ['integer', 'required'],
        ]);

        $abonnement = Abonnement::create([
            'nb_mois' => $inputs['nombre_mois'],
            'user_id' => Auth::id(),
            'typeabonnement_id' => $inputs['type_abonnement']
        ]);

        if(empty($abonnement)) {
            return 'nok';
        }

        return 'ok';
    }

    public function profile() {
        $user = Auth::user();

        return view('frontEnd.Abonnnements.editAbonnement', compact('user'));
    }

    public function update(Request $request, User $user) {
        if(is_null($user)) {
            return redirect(route());
        }

        $inputs = $request->all();
        $request->validate([
            'nom' => ['required', 'string', 'max:255'],
            'prenom' => ['required', 'string', 'max:255'],
            'login' => ['required', 'string', 'max:255','unique:users'],
            'telephone' => ['string','unique:users', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'pays' => ['string', 'required'],
        ]);

        $user->update($inputs);

        return redirect(route('espace.abonne.index'));
    }
}
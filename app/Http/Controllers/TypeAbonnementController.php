<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\TypeAbonnement;
use Illuminate\Http\Request;

class TypeAbonnementController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the TypeAbonnement.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $typeAbonnements = TypeAbonnement::all();

        return view('admin.type_abonnements.index')
            ->with('typeAbonnements', $typeAbonnements);
    }

    /**
     * Show the form for creating a new TypeAbonnement.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.type_abonnements.create');
    }

    /**
     * Store a newly created TypeAbonnement in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $typeAbonnement = TypeAbonnement::create([
            'libelle' => $input['libelle'],
            'mt_mensuel' => $input['mt_mensuel'],
            'taux_echange' => $input['taux_echange']
        ]);

        return redirect(route('admin.typeAbonnements.index'));
    }

    /**
     * Display the specified TypeAbonnement.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $typeAbonnement = TypeAbonnement::findOrFail($id);

        if (empty($typeAbonnement)) {
            return redirect(route('admin.typeAbonnements.index'));
        }

        return view('admin.type_abonnements.show')->with('typeAbonnement', $typeAbonnement);
    }

    /**
     * Show the form for editing the specified TypeAbonnement.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $typeAbonnement = TypeAbonnement::findOrFail($id);

        if (empty($typeAbonnement)) {
            return redirect(route('admin.typeAbonnements.index'));
        }

        return view('admin.type_abonnements.edit')->with('typeAbonnement', $typeAbonnement);
    }

    /**
     * Update the specified TypeAbonnement in storage.
     *
     * @param  int              $id
     * @param Request $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $typeAbonnement = TypeAbonnement::findOrFail($id);

        if (empty($typeAbonnement)) {
            return redirect(route('admin.typeAbonnements.index'));
        }

        $inputs = $request->all();
        $typeAbonnement->libelle = $inputs['libelle'];
        $typeAbonnement->mt_mensuel = $inputs['mt_mensuel'];
        $typeAbonnement->taux_echange = $inputs['taux_echange'];

        $typeAbonnement->save();

        return redirect(route('admin.typeAbonnements.index'));
    }

    /**
     * Remove the specified TypeAbonnement from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $typeAbonnement = TypeAbonnement::findOrFail($id);

        if (empty($typeAbonnement)) {
            return redirect(route('admin.typeAbonnements.index'));
        }

        $typeAbonnement->delete($id);

        return redirect(route('admin.typeAbonnements.index'));
    }
}

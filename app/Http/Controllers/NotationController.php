<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Notation;
use Illuminate\Http\Request;

class NotationController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the Notation.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $notations = Notation::all();

        return view('admin.notations.index')
            ->with('notations', $notations);
    }

    /**
     * Show the form for creating a new Notation.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.notations.create');
    }

    /**
     * Store a newly created Notation in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $notation = Notation::create([

        ]);

        return redirect(route('admin.notations.index'));
    }

    /**
     * Display the specified Notation.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $notation = Notation::findWithoutFail($id);

        if (empty($notation)) {
            return redirect(route('admin.notations.index'));
        }

        return view('admin.notations.show')->with('notation', $notation);
    }

    /**
     * Show the form for editing the specified Notation.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $notation = Notation::findWithoutFail($id);

        if (empty($notation)) {
            return redirect(route('admin.notations.index'));
        }

        return view('admin.notations.edit')->with('notation', $notation);
    }

    /**
     * Update the specified Notation in storage.
     *
     * @param  int              $id
     * @param Request $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $notation = Notation::findWithoutFail($id);

        if (empty($notation)) {
            return redirect(route('admin.notations.index'));
        }

        $inputs = $request->all();

        return redirect(route('admin.notations.index'));
    }

    /**
     * Remove the specified Notation from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $notation = Notation::findWithoutFail($id);

        if (empty($notation)) {
            return redirect(route('admin.notations.index'));
        }

        $notation->delete($id);

        return redirect(route('admin.notations.index'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Categorie;
use Illuminate\Http\Request;

class CategorieController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the Categorie.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $categories = Categorie::all();

        return view('admin.categories.index')
            ->with('categories', $categories);
    }

    /**
     * Show the form for creating a new Categorie.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created Categorie in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
       
        $categorie = Categorie::create([
            'libelle' => $request->get('libelle'),
        ]);

        return redirect(route('admin.categories.index'));
    }

    /**
     * Display the specified Categorie.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $categorie = Categorie::find($id);

        if (empty($categorie)) {
            return redirect(route('admin.categories.index'));
        }

        return view('admin.categories.show')->with('categorie', $categorie);
    }

    /**
     * Show the form for editing the specified Categorie.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $categorie = Categorie::findOrFail($id);

        if (empty($categorie)) {
            return redirect(route('admin.categories.index'));
        }

        return view('admin.categories.edit')->with('categorie', $categorie);
    }

    /**
     * Update the specified Categorie in storage.
     *
     * @param  int              $id
     * @param Request $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $categorie = Categorie::findOrFail($id);

        $request->validate([
            'libelle' => 'required',
        ]);

        if (empty($categorie)) {
            return redirect(route('admin.categories.index'));
        }
        else
        {
            $inputs = $request->all();
            $categorie->update($inputs);
        }
        
        return redirect(route('admin.categories.index'));
    }

    /**
     * Remove the specified Categorie from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $categorie = Categorie::find($id);

        if (empty($categorie)) {

            return redirect(route('admin.categories.index'));
        }

        $categorie->delete($id);

        return redirect(route('admin.categories.index'));
    }
}

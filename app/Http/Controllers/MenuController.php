<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categorie;
use App\Models\Film;
use DemeterChain\C;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()) {
            return redirect(url('/'));
        } else {
            return redirect(route('login'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
         //Recuperer les categories pour le menu
        $categories = Categorie::get();
        // //recupere le nom de la categories selectionnée
        $macategorie = Categorie::find($id);

        $news = DB::table('films')
            ->join('categorie_has_films', 'films.id', '=', 'categorie_has_films.film_id')
            ->join('categories', 'categories.id', '=', 'categorie_has_films.categorie_id')
            ->where('categorie_has_films.categorie_id', '=', $macategorie->id)
            ->select('films.*')->get();

        $mosts = DB::table('films')
            ->join('categorie_has_films', 'films.id', '=', 'categorie_has_films.film_id')
            ->join('categories', 'categories.id', '=', 'categorie_has_films.categorie_id')
            ->where('categorie_has_films.categorie_id', '=', $macategorie->id)
            ->select('films.*')->get();

        if ($request->ajax()) {
            //une requette de pagination
            return view('films.listsFilmsByCategorie', compact('categories', 'news', 'mosts', 'macategorie'))->render();
            //dd('fhh');
        }
        return view('films.listsFilmsByCategorie', compact('categories', 'news', 'mosts', 'macategorie'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Promo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PromoController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Display a listing of the Categorie.
     *
     * @param Request $request
     * @return Response
     * 
     */

    public function index(Request $request)
    {
        $promos = Promo::all();

        return view('admin.promos.index')
            ->with('promos', $promos);
    }

    /**
     * Show the form for creating a new Categorie.
     *
     * @return Response
     * 
     */

    public function create()
    {
        return view('admin.promos.create')->with('promo', null);
    }

    /**
     * Store a newly created Categorie in storage.
     *
     * @param Request $request
     *
     * @return Response
     * 
     */

    public function store(Request $request)
    {

        $promo = Promo::create([
            'libelle_promo' => $request->get('libelle_promo'),
            'description' => $request->get('description'),
            'date_debut' => $request->get('date_debut'),
            'date_fin' => $request->get('date_fin')
        ]);

        return redirect(route('admin.promos.index'));
    }

    /**
     * Display the specified Categorie.
     *
     * @param  int $id
     *
     * @return Response
     * 
     */

    public function show($id)
    {
        $promo = Promo::findOrFail($id);

        if (empty($promo)) {
            return redirect(route('admin.promos.index'));
        }

        return view('admin.promos.show')->with('promo', $promo);
    }

    /**
     * Show the form for editing the specified Categorie.
     *
     * @param  int $id
     *
     * @return Response
     * 
     */

    public function edit($id)
    {
        $promo = Promo::findOrFail($id);

        if (empty($promo)) {
            return redirect(route('admin.promos.index'));
        }

        return view('admin.promos.edit')->with('promo', $promo);
    }

    /**
     * Update the specified Categorie in storage.
     *
     * @param  int              $id
     * @param Request $request
     *
     * @return Response
     * 
     */

    public function update($id, Request $request)
    {
        $promo = Promo::findOrFail($id);

        $request->validate([
            'libelle_promo' => 'required',
            'description' => 'required',
            'date_debut' => 'required',
            'date_fin' => 'required',
        ]);

        if (empty($promo)) {
            return redirect(route('admin.promos.index'));
        } else {
            $inputs = $request->all();
            $promo->update($inputs);
        }

        return redirect(route('admin.promos.index'));
    }

    /**
     * Remove the specified Categorie from storage.
     *
     * @param  int $id
     *
     * @return Response
     * 
     */

    public function destroy($id)
    {
        $promo = Promo::find($id);

        if (empty($promo)) {

            return redirect(route('admin.promos.index'));
        }

        $promo->delete($id);

        return redirect(route('admin.promos.index'));
    }
}
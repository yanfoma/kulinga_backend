<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categorie;
use App\Models\Film;
use Illuminate\Support\Facades\Auth;
use App\Models\Abonnement;
use App\Models\Promo;
use Illuminate\Support\Facades\DB;

class RegarderFilmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //La liste des categories pour le menu
        $categories = Categorie::get();
        //La categorie du films selectionne
        $cate = DB::table('categories')
            ->join('categorie_has_films', 'categories.id', '=', 'categorie_has_films.categorie_id')
            ->join('films', 'films.id', '=', 'categorie_has_films.film_id')
            ->where('categorie_has_films.film_id', '=', $id)
            ->select('categories.*')->get()->first();
        //dd($cate->id);
        if (Auth::user()) {
            $promo = Promo::get()->last();
            if (isset($promo) && $promo->date_debut <= date_create(date_default_timezone_get()) && $promo->date_fin >= date_create(date_default_timezone_get())) {
                $unepromo = 1;
                $monfilm = Film::findOrFail($id);
                     //la liste des films de la categorie
                $films = DB::table('films')
                    ->join('categorie_has_films', 'films.id', '=', 'categorie_has_films.film_id')
                    ->join('categories', 'categories.id', '=', 'categorie_has_films.categorie_id')
                    ->where('categorie_has_films.categorie_id', '=', $cate->id)
                    ->select('films.*')->get();

                     //vues du film
                if (isset($monfilm)) {
                    $vue = $monfilm->nb_vues + 1;
                    Film::where('id', $id)->update(['nb_vues' => $vue]);
                }
                return view('films.showFilms', compact('categories', 'monfilm', 'films', 'unepromo'));
            } else {
                $ab = Abonnement::where('user_id', Auth::user()->id)->get()->last();
                if (isset($ab) && date_diff(date_create(date_default_timezone_get()), $ab->created_at)->m <= $ab->nb_mois) {
                    $monfilm = Film::findOrFail($id);

                        //la liste des films
                    $films = DB::table('films')
                        ->join('categorie_has_films', 'films.id', '=', 'categorie_has_films.film_id')
                        ->join('categories', 'categories.id', '=', 'categorie_has_films.categorie_id')
                        ->where('categorie_has_films.categorie_id', '=', $cate->id)
                        ->select('films.*')->get();

                        //vues du film
                    if (isset($monfilm)) {
                        $vue = $monfilm->nb_vues + 1;
                        Film::where('id', $id)->update(['nb_vues' => $vue]);
                    }
                    return view('films.showFilms', compact('categories', 'monfilm', 'films', 'isConnect'));
                }
            }
            return redirect(route('espace.abonne.home'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
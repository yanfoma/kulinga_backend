<?php

namespace App\Http\Controllers;

use App\Models\TypeFilm;
use Illuminate\Http\Request;

class TypeFilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.type_film.index')->with('typeFilms', TypeFilm::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.type_film.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();

        $typeFilm = TypeFilm::create([
            'libelle_type' => $inputs['libelle_type'],
        ]);

        return redirect(route('type_film.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TypeFilm  $typeFilm
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $typeFilm = TypeFilm::findOrFail($id);

        if (empty($typeFilm)) {
            return redirect(route('type_film.index'));
        }

        return view('admin.type_film.show')->with('typeFilm', $typeFilm);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TypeFilm  $typeFilm
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $typeFilm = TypeFilm::findOrFail($id);

        if (empty($typeFilm)) {
            return redirect(route('type_film.index'));
        }

        return view('admin.type_film.edit')->with('typeFilm', $typeFilm);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TypeFilm  $typeFilm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $typeFilm = TypeFilm::findOrFail($id);

        if (empty($typeFilm)) {
            return redirect(route('type_film.index'));
        }

        $inputs = $request->all();
        $typeFilm->libelle_type = $inputs['libelle_type'];

        $typeFilm->save();

        return redirect(route('type_film.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TypeFilm  $typeFilm
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $typeFilm = TypeFilm::findOrFail($id);

        if (empty($typeFilm)) {
            return redirect(route('type_film.index'));
        }

        $typeFilm->delete($id);

        return redirect(route('type_film.index'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\AvisAbonne;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AvisabonneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if (!is_null($user) && !$user->hasAnyRole('Admin')) {
            $avis_abonnes = AvisAbonne::with('user')->where('user_id', $user->id)
                ->with('user')
                ->simplePaginate(10);
        } else {
            $avis_abonnes = AvisAbonne::with('user')->simplePaginate(10);
        }

        if (!is_null($user) && !Auth::user()->hasAnyRole('Admin')) {
            return view('frontEnd.avis_abonne.index', compact('avis_abonnes'));
        }

        return view('admin.avis_abonne.index', compact('avis_abonnes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if (!is_null(Auth::user()) && !Auth::user()->hasAnyRole('Admin')) {
            $avis_abonnes = AvisAbonne::with('user')
            ->with('user')
            ->simplePaginate(3);
         return view('frontEnd.avis_abonne.create', compact('avis_abonnes'));
        }

    return view('admin.avis_abonne.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
           'titre' => ['string', 'required'] ,
            'message' => ['string', 'required']
        ]);
        AvisAbonne::create([
            'titre' => $request->get('titre'),
            'message' => $request->get('message'),
            'user_id' =>Auth::user()->id,
        ]);

        if($request->ajax()) {
            return 'success';
        }

        return redirect(route('avis_abonne.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AvisAbonne  $avisAbonne
     * @return \Illuminate\Http\Response
     */
    public function show(AvisAbonne $avis_abonne)
    {
        if(empty($avis_abonne)) {
            return redirect(route('avis_abonne.index'));
        }

        if (!is_null(Auth::user()) && !Auth::user()->hasAnyRole('Admin')) {
            return view('frontEnd.avis_abonne.show', compact('avis_abonne'));
        }
        return view('admin.avis_abonne.show', compact('avis_abonne'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AvisAbonne  $avis_abonne
     * @return \Illuminate\Http\Response
     */
    public function edit(AvisAbonne $avis_abonne)
    {
        if(empty($avis_abonne)) {
            return redirect(route('avis_abonne.index'));
        }

        if (!is_null(Auth::user()) && !Auth::user()->hasAnyRole('Admin')) {
            return view('frontEnd.avis_abonne.edit', compact('avis_abonne'));
        }
        return view('admin.avis_abonne.edit', compact('avis_abonne'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AvisAbonne  $avis_abonne
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AvisAbonne $avis_abonne)
    {
        $request->validate([
            'titre' => ['string', 'required'] ,
            'message' => ['string', 'required']
        ]);

        if(empty($avis_abonne)) {
            return redirect(route('avis_abonne.index'));
        }

        $avis_abonne->titre = $request->get('titre');
        $avis_abonne->message = $request->get('message');
        $avis_abonne->user_id = Auth::id();
        $avis_abonne->save();

        return redirect(route('avis_abonne.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AvisAbonne  $avisAbonne
     * @return \Illuminate\Http\Response
     */
    public function destroy(AvisAbonne $avis_abonne)
    {
        if (empty($avis_abonne)) {
            return redirect(route('avis_abonne.index'));
        }

        try {
            $avis_abonne->delete();
        } catch (\Exception $e) {
        }

        return redirect(route('avis_abonne.index'));
    }
}

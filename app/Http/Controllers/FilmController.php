<?php

namespace App\Http\Controllers;

use App\Models\Auteur;
use App\Models\Categorie;
use App\Models\Film;
use App\Models\PrixFilm;
use Illuminate\Support\Facades\DB;
use App\Models\TypeFilm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use File;

class FilmController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the Film.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        /* $films = Film::all()->simplePaginate(5); */
        $films =Film::paginate(4);
        $prixFilms=PrixFilm::all();

        return view('admin.films.index')->with(['films'=>$films,'prixFilms'=>$prixFilms]);
    }

    /**
     * Show the form for creating a new Film.
     *
     * @return Response
     */
    public function create()
    {
        $categories = Categorie::all();
        $typeFilms = TypeFilm::all();
        $auteurs = Auteur::all();
        return view('admin.films.create')->with(['film' => null, 'categories' => $categories, 'typeFilms' => $typeFilms, 'auteurs' => $auteurs]);
    }

    /**
     * Store a newly created Film in storage.
     *
     * @param CreateFilmRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $filmData = $this->uploadFilm($request);
        $posterData = $this->uploadPoster($request);
        $duration = $this->getDuration('uploads/films/' . $filmData['film_key']);

        $film = Film::create([
            'titre' => $request->get('titre'),
            'description' => $request->get('description'),
            'date_realisation' => $request->get('date_realisation'),
            'duree_film' => $duration,
            'url_film' => $filmData['film_curl'],
            'pub_key_film' => $filmData['film_key'],
            'url_image' => $posterData['poster_url'],
            'pub_key_image' => $posterData['poster_key'],
            'user_id' => Auth::id(),
            'auteur_id' => $request->get('auteur_id'),
            'type_film_id' => $request->get('type_film_id'),
            'contact_distributeur' => $request->get('contact_distributeur'),
            'identite_distributeur' => $request->get('identite_distributeur'),
            'adresse_distributeur' => $request->get('adresse_distributeur'),
        ]);
        $categorie_id = $request->get('categorie_id');
        $film->categories()->sync($categorie_id);

        return redirect(route('admin.films.index'));
    }

    /**
     * Display the specified Film.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $film = Film::findOrFail($id);

        if (empty($film)) {
            return redirect(route('admin.films.index'));
        }

        return view('admin.films.show')->with('film', $film);
    }

    /**
     * Show the form for editing the specified Film.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $film = Film::findOrFail($id);
        $categories = Categorie::all();
        $typeFilms = TypeFilm::all();
        $auteurs = Auteur::all();

        if (empty($film)) {
            return redirect(route('admin.films.index'));
        }

        return view('admin.films.edit')->with(['film' => $film, 'categories' => $categories, 'typeFilms' => $typeFilms, 'auteurs' => $auteurs]);
    }

    /**
     * Update the specified Film in storage.
     *
     * @param  int $id
     * @param Request $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $film = Film::findOrFail($id);

        if (empty($film)) {

            return redirect(route('admin.films.index'));
        }

        if ($request->hasFile('url_image') && $request->hasFile('url_film')) {
            $filmData = $this->uploadFilm($request);
            $posterData = $this->uploadPoster($request);
            $duration = $this->getDuration('uploads/films/' . $filmData['film_key']);
            $this->deleteFileFilm($film->pub_key_film);
            $this->deleteFileImage($film->pub_key_image);

            $film->titre = $request->get('titre');
            $film->description = $request->get('description');
            $film->date_realisation = $request->get('date_realisation');
            $film->duree_film = $duration;
            $film->type_film_id = $request->get('type_film_id');
            $film->url_film = $filmData['film_curl'];
            $film->pub_key_film = $filmData['film_key'];
            $film->url_image = $posterData['poster_url'];
            $film->pub_key_image = $posterData['poster_key'];
            $film->auteur_id = $request->get('auteur_id');
            $film->type_film_id = $request->get('type_film_id');
            $film->contact_distributeur = $request->get('contact_distributeur');
            $film->identite_distributeur = $request->get('identite_distributeur');
            $film->adresse_distributeur = $request->get('adresse_distributeur');
            $film->save();
            
            $categorie_id = $request->get('categorie_id');
            $film->categories()->sync($categorie_id);
        }

        if (!empty($request->file('url_image')) && empty($request->file('url_film'))) {

            $posterData = $this->uploadPoster($request);
            $this->deleteFileImage($film->pub_key_image);

            $film->titre = $request->get('titre');
            $film->description = $request->get('description');
            $film->date_realisation = $request->get('date_realisation');
            //'duree_film' => $duration,
            $film->type_film_id = $request->get('type_film_id');
            $film->url_image = $posterData['poster_url'];
            $film->pub_key_image = $posterData['poster_key'];
            $film->auteur_id = $request->get('auteur_id');
            $film->type_film_id = $request->get('type_film_id');
            $film->contact_distributeur = $request->get('contact_distributeur');
            $film->identite_distributeur = $request->get('identite_distributeur');
            $film->adresse_distributeur = $request->get('adresse_distributeur');
            $film->save();

            $categorie_id = $request->get('categorie_id');
            $film->categories()->sync($categorie_id);
        }

        if (empty($request->file('url_image')) && !empty($request->file('url_film'))) {

            $filmData = $this->uploadFilm($request);
            $duration = $this->getDuration('uploads/films/' . $filmData['film_key']);
            $this->deleteFileFilm($film->pub_key_film);

            $film->titre = $request->titre;
            $film->description = $request->description;
            $film->date_realisation = $request->date_realisation;
            $film->duree_film = $duration;
            $film->type_film_id = $request->get('type_film_id');
            $film->url_film = $filmData['film_curl'];
            $film->pub_key_film = $filmData['film_key'];
            $film->auteur_id = $request->get('auteur_id');
            $film->type_film_id = $request->get('type_film_id');
            $film->contact_distributeur = $request->get('contact_distributeur');
            $film->identite_distributeur = $request->get('identite_distributeur');
            $film->adresse_distributeur = $request->get('adresse_distributeur');
            $film->save();
            $categorie_id = $request->get('categorie_id');
            $film->categories()->sync($categorie_id);
        }

        if (empty($request->file('url_image')) && empty($request->file('url_film'))) {

            $film->titre = $request->get('titre');
            $film->description = $request->get('description');
            $film->date_realisation = $request->get('date_realisation');
            //'duree_film' => $duration,
            $film->type_film_id = $request->get('type_film_id');
            $film->auteur_id = $request->get('auteur_id');
            $film->contact_distributeur = $request->get('contact_distributeur');
            $film->identite_distributeur = $request->get('identite_distributeur');
            $film->adresse_distributeur = $request->get('adresse_distributeur');
            $film->save();

            $categorie_id = $request->get('categorie_id');
            $film->categories()->sync($categorie_id);
        }

        return redirect(route('admin.films.index'));
    }

    /**
     * Remove the specified Film from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $film = Film::findOrFail($id);

        if (empty($film)) {
            return redirect(route('admin.films.index'));
        }
        $this->deleteFileFilm($film->pub_key_film);
        $this->deleteFileImage($film->pub_key_image);
        $film->delete($id);
        return redirect(route('admin.films.index'));
    }


    private function uploadFilm(Request $request)
    {
        $uploadDir = public_path('\\uploads\\films\\');
        $film = $request->file('url_film');
        $film_key = $film->getClientOriginalName();
        $result = $film->move($uploadDir, $film_key);
        return array('film_curl' => '\\uploads\\films\\' . $film_key, 'film_key' => $result->getFilename(), 'film_size' => $result->getSize());
    }


    private function uploadPoster(Request $request)
    {
        $uploadDir = public_path('\\uploads\\images\\');
        $poster = $request->file('url_image');
        $image_key = $poster->getClientOriginalName();
        $result = $poster->move($uploadDir, $image_key);
        return array('poster_url' => '\\uploads\\images\\' . $image_key, 'poster_key' => $result->getFilename());
    }

    public function getDuration($video)
    {
        $getID3 = new \getID3;
        $file = $getID3->analyze($video);
        $playtime_seconds = $file['playtime_seconds'];
        $duration = date('H:i:s', $playtime_seconds);
        return $duration;
    }

    public function deleteFileFilm($film)
    {
        $destinationPathFilm = 'uploads/films/';
        File::delete($destinationPathFilm . $film);
    }

    public function deleteFileImage($image)
    {
        $destinationPathImage = 'uploads/images/';
        File::delete($destinationPathImage . $image);
    }
}

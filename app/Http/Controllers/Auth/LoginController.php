<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

//    protected $redirectTo = '/kulinga/espace_abonne';

    protected function username()
    {
        return 'login';
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function logout(Request $request)
    {
        $user = $request->user();
        Auth::logout();
        if ($user->hasAnyRole('subscriber')) {
            return redirect('/');
        }
        return redirect()->route('login');
    }

    protected function authenticated(Request $request, $user)
    {
        if ($user->hasRole('subscriber')) {
            return redirect()->route('espace.abonne.home');
        }

        if ($user->hasRole('Admin')) {
            return redirect()->route('admin.dashboard');
        }

        return redirect('/');
    }
}

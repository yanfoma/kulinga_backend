<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Abonnement;
use Illuminate\Http\Request;

class AbonnementController extends Controller
{

    public function __construct()
    {
        
    }

    /**
     * Display a listing of the Abonnement.
     *
     * @return Response
     */
    public function index()
    {
        $abonnements = Abonnement::all();

        return view('admin.abonnements.index')
            ->with('abonnements', $abonnements);
    }

    /**
     * Show the form for creating a new Abonnement.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.abonnements.create');
    }

    /**
     * Store a newly created Abonnement in storage.
     *
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        Abonnement::create([

        ]);

        return redirect(route('admin.abonnements.index'));
    }

    /**
     * Display the specified Abonnement.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $abonnement = Abonnement::findOrFail($id);

        if (empty($abonnement)) {
            return redirect(route('admin.abonnements.index'));
        }

        return view('admin.abonnements.show')->with('abonnement', $abonnement);
    }

    /**
     * Show the form for editing the specified Abonnement.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $abonnement = Abonnement::findOrFail($id);

        if (empty($abonnement)) {
            return redirect(route('admin.abonnements.index'));
        }

        return view('admin.abonnements.edit')->with('abonnement', $abonnement);
    }

    /**
     * Update the specified Abonnement in storage.
     *
     * @param  int              $id
     * @param UpdateAbonnementRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $abonnement = Abonnement::findOrFail($id);

        if (empty($abonnement)) {
            return redirect(route('admin.abonnements.index'));
        }
        $inputs = $request->all();


        return redirect(route('admin.abonnements.index'));
    }

    /**
     * Remove the specified Abonnement from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $abonnement = Abonnement::findOrFail($id);

        if (empty($abonnement)) {
            return redirect(route('admin.abonnements.index'));
        }

        $abonnement->delete($id);

        return redirect(route('admin.abonnements.index'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\PrixFilm;
use App\Models\Film;
use Illuminate\Http\Request;

class prixFilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $prixFilms = PrixFilm::where('film_id',$id)->get();
        $film=Film::findOrFail($id);

        return view('admin.prixFilms.index')->with(['prixFilms'=>$prixFilms,'film'=>$film]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $film=Film::findOrFail($id);
        return view('admin.prixFilms.create')->with(['film'=>$film]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'titre_prix' => 'required'
       ]);

        $film_id=$request->get('film_id');
        $prixFilm = PrixFilm::create([
            'titre_prix' => $request->get('titre_prix'),
            'descritption_prix' => $request->get('description_prix'),
            'valeur_prix' => $request->get('valeur_prix'),
            'film_id' => $request->get('film_id')
        ]);
        $film=Film::findOrFail($film_id);
        return redirect(route('admin.prixFilms.index',[$film->id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PrixFilm  $prixFilm
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $prixFilm = PrixFilm::findOrFail($id);
        $film = Film::findOrFail($prixFilm->film_id);
        dd($film);
        if (empty($prixFilm)) {
            return redirect(route('admin.prixFilms.index',[$film->id]));
        }
        return view('admin.prixFilms.show')->with(['prixFilm'=>$prixFilm,'film'=>$film]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PrixFilm  $prixFilm
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $prixFilm = PrixFilm::findOrFail($id);
        $film=Film::findOrFail($prixFilm->film_id);
        if (empty($prixFilm)) {
            return redirect(route('admin.prixFilms.index',[$film->id]));
        }
        return view('admin.prixFilms.edit')->with(['prixFilm'=>$prixFilm, 'film'=>$film]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PrixFilm  $prixFilm
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        
        $prixFilm = PrixFilm::findOrFail($id);
        $film=Film::findOrFail($prixFilm->film_id);
        $request->validate([
             'titre_prix' => 'required',
             'description_prix' => 'required',
        ]);

        if (empty($prixFilm)) {
            return redirect(route('admin.prixFilms.index',[$film->id]));
        }
        else
        {
            $inputs = $request->all();
            $prixFilm->update($inputs);
        }
        
        return redirect(route('admin.prixFilms.index',[$film->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PrixFilm  $prixFilm
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prixFilm = PrixFilm::findOrFail($id);
        $film=Film::findOrFail($prixFilm->film_id);
        if (empty($prixFilm)) {

            return redirect(route('admin.prixFilms.index',[$film->id]));
        }

        $prixFilm->delete($id);

        return redirect(route('admin.prixFilms.index',[$film->id]));
    }
}

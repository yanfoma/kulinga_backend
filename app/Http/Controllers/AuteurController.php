<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Auteur;
use Illuminate\Http\Request;

class AuteurController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the Auteur.
     *
     * @param Request $request
     * @return Response
     */
    public function index()
    {
        $auteurs = Auteur::all();

        return view('admin.auteurs.index')
            ->with('auteurs', $auteurs);
    }

    /**
     * Show the form for creating a new Auteur.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.auteurs.create', ['auteur' => null]);
    }

    /**
     * Store a newly created Auteur in storage.
     *
     * @param CreateAuteurRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $auteur = Auteur::create([
            'nom'=> $request->get('nom'),
            'pays'=> $request->get('pays')
        ]);

        return redirect(route('admin.auteurs.index'));
    }

    /**
     * Display the specified Auteur.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $auteur = Auteur::find($id);

        if (empty($auteur)) {
            return redirect(route('admin.auteurs.index'));
        }

        return view('admin.auteurs.show')->with('auteur', $auteur);
    }

    /**
     * Show the form for editing the specified Auteur.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $auteur = Auteur::find($id);

        if (empty($auteur)) {
            return redirect(route('admin.auteurs.index'));
        }

        return view('admin.auteurs.edit')->with('auteur', $auteur);
    }

    /**
     * Update the specified Auteur in storage.
     *
     * @param  int              $id
     * @param Request $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $auteur = Auteur::find($id);

        if (empty($auteur)) {
            return redirect(route('admin.auteurs.index'));
        }
        else
        {
            $inputs = $request->all();
            $auteur->update($inputs);
        }

        return redirect(route('admin.auteurs.index'));
    }

    /**
     * Remove the specified Auteur from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $auteur = Auteur::find($id);

        if (empty($auteur)) {
            return redirect(route('admin.auteurs.index'));
        }

        $auteur->destroy($id);

        return redirect(route('admin.auteurs.index'));
    }
}

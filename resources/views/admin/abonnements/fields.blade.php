<!-- Nb Mois Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nb_mois', 'Nb Mois:') !!}
    {!! Form::number('nb_mois', null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Typeabonnement Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('typeabonnement_id', 'Typeabonnement Id:') !!}
    {!! Form::number('typeabonnement_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.abonnements.index') !!}" class="btn btn-default">Cancel</a>
</div>

<table class="table table-responsive" id="abonnements-table">
    <thead>
        <tr>
            <th>Nb Mois</th>
        <th>User Id</th>
        <th>Typeabonnement Id</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($abonnements as $abonnement)
        <tr>
            <td>{!! $abonnement->nb_mois !!}</td>
            <td>{!! $abonnement->user_id !!}</td>
            <td>{!! $abonnement->typeabonnement_id !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.abonnements.destroy', $abonnement->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.abonnements.show', [$abonnement->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.abonnements.edit', [$abonnement->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $abonnement->id !!}</p>
</div>

<!-- Nb Mois Field -->
<div class="form-group">
    {!! Form::label('nb_mois', 'Nb Mois:') !!}
    <p>{!! $abonnement->nb_mois !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $abonnement->user_id !!}</p>
</div>

<!-- Typeabonnement Id Field -->
<div class="form-group">
    {!! Form::label('typeabonnement_id', 'Typeabonnement Id:') !!}
    <p>{!! $abonnement->typeabonnement_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $abonnement->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $abonnement->updated_at !!}</p>
</div>


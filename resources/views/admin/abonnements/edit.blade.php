@extends('layouts.adminApp')

@section('content')
    <section class="content-header">
        <h1>
            Abonnement
        </h1>
   </section>
   <div class="content">
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($abonnement, ['route' => ['admin.abonnements.update', $abonnement->id], 'method' => 'patch']) !!}

                        @include('admin.abonnements.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
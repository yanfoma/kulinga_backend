<table class="table table-responsive" id="users-table">
    <thead>
        <tr>
            <th>Nom</th>
        <th>Prenom</th>
        <th>Telephone</th>
        <th>Login</th>
        <th>Email</th>
        <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($users as $users)
        <tr>
            <td>{!! $users->nom !!}</td>
            <td>{!! $users->prenom !!}</td>
            <td>{!! $users->telephone !!}</td>
            <td>{!! $users->login !!}</td>
            <td>{!! $users->email !!}</td>
            <td>
                <div class='btn-group'>
                    <a href="{!! route('admin.users.show', [$users->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.users.edit', [$users->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::open(['route' => ['admin.users.destroy', $users->id], 'method' => 'delete']) !!}
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@extends('layouts.adminApp')

@section('content')
    <section class="content-header">
        <h1>
            Users
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::model($users, ['route' => ['admin.users.update', $users->id], 'method' => 'patch']) !!}

                    @include('admin.users.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function () {
            $('#pays').select2({
                theme: 'classic'
            })
        })
    </script>
@endsection
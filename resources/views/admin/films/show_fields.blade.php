

<div class="col-md-8">
<!-- Titre Field -->
<div class="form-group">
        {!! Form::label('titre', 'Titre:') !!}
        <p>{!! $film->titre !!}</p>
    </div>
    
    <!-- Description Field -->
    <div class="form-group">
        {!! Form::label('description', 'Description:') !!}
        <p>{!! $film->description !!}</p>
    </div>
    
    <!-- Date Realisation Field -->
    <div class="form-group">
        {!! Form::label('date_realisation', 'Date Realisation:') !!}
        <?php $date= strtotime($film->date_realisation) ?>
        <p>{!! $dateRelise= date("d/m/y",$date) !!}</p>
    </div>
    
    <!-- Duree Film Field -->
    <div class="form-group">
        {!! Form::label('duree_film', 'Duree Film:') !!}
        <?php $date= strtotime($film->duree_film) ?>
        <p>{!! $dateRelise= date("H:i:s",$date) !!}</p>
    </div>
    
    <!-- Nb Vues Field -->
    <div class="form-group">
        {!! Form::label('nb_vues', 'Nb Vues:') !!}
        <p>{!! $film->nb_vues !!}</p>
    </div>
    
    <!-- Type Film Field -->
    <div class="form-group">
        {!! Form::label('type_film', 'Type Film:') !!}
        <p>{!! $film->type_film !!}</p>
    </div>
</div>
<div class="col-md-4">
        <img class="img-responsive img-thumbnail" src="{{ '/uploads/images/'.$film->pub_key_image }}" 
        alt="{{ $film->titre }}">
        <div class="caption">
                <h4>{{ $film->titre }}</h4>
        </div>
</div>

{{-- <!-- Url Film Field -->
<div class="form-group">
    {!! Form::label('url_film', 'Url Film:') !!}
    <p>{!! $film->url_film !!}</p>
</div>

<!-- Pub Key Film Field -->
<div class="form-group">
    {!! Form::label('pub_key_film', 'Pub Key Film:') !!}
    <p>{!! $film->pub_key_film !!}</p>
</div>

<!-- Url Image Field -->
<div class="form-group">
    {!! Form::label('url_image', 'Url Image:') !!}
    <p>{!! $film->url_image !!}</p>
</div>

<!-- Pub Key Image Field -->
<div class="form-group">
    {!! Form::label('pub_key_image', 'Pub Key Image:') !!}
    <p>{!! $film->pub_key_image !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $film->user_id !!}</p>
</div>

<!-- Auteur Id Field -->
<div class="form-group">
    {!! Form::label('auteur_id', 'Auteur Id:') !!}
    <p>{!! $film->auteur_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $film->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $film->updated_at !!}</p>
</div>

 --}}
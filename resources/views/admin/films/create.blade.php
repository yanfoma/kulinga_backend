@extends('layouts.adminApp')

@section('content')
    <section class="content-header">
        <h1>
            Film
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'admin.films.store', 'file'=>true, 'enctype' => 'multipart/form-data']) !!}
                    {{ csrf_field() }}
                    @include('admin.films.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function () {
            $('#auteurs').select2({
                theme: 'classic'
            });
            $('#type_film_id').select2({
                theme: 'classic'
            });
            $('#categories').select2({
                theme: 'classic'
            });
            $('.flatpickr').flatpickr({

            });
        })
    </script>
@endsection
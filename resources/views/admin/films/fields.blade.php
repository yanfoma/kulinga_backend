<div class="container-fluid">
    <!-- Titre Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('titre', 'Titre du film:') !!}
        {!! Form::text('titre', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Description Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('description', 'Description du film:') !!}
        {!! Form::text('description', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="container-fluid">
    <!-- Date Realisation Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('date_realisation', 'Date de réalisation:') !!}
        {!! Form::date('date_realisation', 'dd-mm-yyyy', ['class' => 'form-control flatpickr']) !!}
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('categorie_film', 'Catégorie du film:') !!}
        <select class="form-control" id="categories" name="categorie_id">
            @foreach($categories as $category)
                <option value="{{$category->id}}"
                     {{--@if ($film != null and $category->id==old('categories', $film->type_film))
                        selected="selected"
                     @endif --}}
                    >{{$category->libelle}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="container-fluid">

    <!-- Type Film Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('type_film_id', 'Type du film:') !!}
        <select class="form-control" id="type_film_id"name="type_film_id">
            @foreach($typeFilms as $typeFilm)
                <option value="{{$typeFilm->id}}"
                 @if ($film != null and $typeFilm->id==old('type_film_id', $film->type_film_id))
                    selected="selected"
                @endif 
                >{{$typeFilm->libelle_type}}</option>
            @endforeach
        </select>
    </div>

    <!-- Auteur Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('auteur_id', 'Auteur du films : ') !!}
        <select class="form-control" id="auteurs" name="auteur_id">
            @foreach($auteurs as $auteur)
                <option value="{{$auteur->id}}"
                    @if ($film != null and $auteur->id==old('auteur_id', $film->auteur_id))
                    selected="selected"
                @endif 
                    >{{$auteur->nom}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="container-fluid">
    <!-- Titre Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('identite_distributeur', 'Identité du distributeur :') !!}
        {!! Form::text('identite_distributeur', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Description Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('adresse_distributeur', 'Adresse du distributeur :') !!}
        {!! Form::text('adresse_distributeur', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="container-fluid">
    <!-- Titre Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('contact_distributeur', 'Contact du distributeur :') !!}
        {!! Form::text('contact_distributeur', null, ['class' => 'form-control']) !!}
    </div>

</div>

<div class="container-fluid">
    <!-- Url Film Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('url_film', 'Choisir le film:') !!}

        {!! Form::file('url_film', ['class' => 'form-control', 'type'=>'file']) !!}
    </div>

    <!-- Url Image Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('url_image', 'Choisir le poster du film:') !!}
        {!! Form::file('url_image', ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.films.index') !!}" class="btn btn-default">Cancel</a>
</div>

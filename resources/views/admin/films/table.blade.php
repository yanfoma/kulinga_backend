
<style>
.imgContainer {
  overflow: hidden;
  width: 200px;
  height: 120px;
}
.imgContainer img {
  width: 200px;
  height: 120px;
}


</style>
{{-- <table class="table table-responsive" id="categories-table">
    <thead>
        <tr>
            <th>Film</th>
            <th>Description</th>
            <th>Prix</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($films as $film)
        <tr>
            <td>
                <div class="imgContainer">
                        <img class="img-responsive img-thumbnail" src="{{ '/uploads/images/'.$film->pub_key_image }}" 
                        alt="{{ $film->titre }}">
                </div>
                 <div class="caption">
                        <h5>{{ $film->titre }}</h5>
                </div>
            </td>
        <td>{!! $film->description !!}</td>
        <td>
            <div class='btn-group text-center'>
                <a href="{!! route('admin.prixFilms.index', [$film->id]) !!}" class='btn btn-default btn-md'><i
                    class="glyphicon glyphicon-eye-open"></i></a>
            </div>
        </td>
            <td>
                    {!! Form::open(['route' => ['admin.films.destroy', $film->id], 'method' => 'delete']) !!}
                    <div class='btn-group text-center'>
                        <a href="{!! route('admin.films.show', [$film->id]) !!}" class='btn btn-default btn-md'><i
                            class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('admin.films.edit', [$film->id]) !!}" class='btn btn-default btn-md'><i
                             class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-md', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
 --}}

@foreach ($films as $film)
<div class="col-sm-3">
    <div class="thumbnail">
            <div class="imgContainer">
                        <img class="img-responsive img-thumbnail" src="{{ '/uploads/images/'.$film->pub_key_image }}" 
                        alt="{{ $film->titre }}">
            </div>
        <div class="caption">
        <h4>{{ $film->titre }}</h4>
        <p>
            <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo{{ $film->id}}">Les prix</button>
                <div id="demo{{ $film->id}}" class="collapse">
                      @foreach ($prixFilms as $prixFilm)
                          @if ($prixFilm->film_id==$film->id)
                              <ul>
                                  <li>{{$prixFilm->titre_prix}}</li>
                              </ul>
                          @endif
                      @endforeach
                      <a href="{!! route('admin.prixFilms.index', [$film->id]) !!}">Ajouter un prix</a>
            </div>
        </p>
        <p>
                {!! Form::open(['route' => ['admin.films.destroy', $film->id], 'method' => 'delete']) !!}
                <div class='btn-group text-center'>
                    <a href="{!! route('admin.films.show', [$film->id]) !!}" class='btn btn-default btn-md'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.films.edit', [$film->id]) !!}" class='btn btn-default btn-md'><i
                                class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-md', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
        </p>
        </div>
    </div>
</div>
@endforeach
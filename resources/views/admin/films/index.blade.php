@extends('layouts.adminApp')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Films</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('admin.films.create') !!}">Add New</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        <div class="clearfix"></div>
        
   
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                        @include('admin.films.table') 
                </div>
                <div class="text-center" > {{ $films->links() }} </div> 
            </div>
        </div>
        <div class="text-center">
        </div>
    </div>
@endsection


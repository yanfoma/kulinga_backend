@extends('layouts.adminApp')

@section('content')
    <section class="content-header">
        <h1>
            Categorie
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'admin.categories.store']) !!}

                        @include('admin.categories.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

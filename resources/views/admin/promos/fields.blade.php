<!-- Libelle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('libelle_promo', 'Libellé de la promo:') !!}
    {!! Form::text('libelle_promo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('date_debut', 'Date de debut:') !!}
    {!! Form::date('date_debut', null, ['class' => 'form-control flatpickr']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('date_fin', 'Date de fin:') !!}
    {!! Form::date('date_fin', null, ['class' => 'form-control flatpickr']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.promos.index') !!}" class="btn btn-default">Cancel</a>
</div>

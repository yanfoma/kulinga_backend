<!-- Id Field -->
{{-- <div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $categorie->id !!}</p>
</div>
 --}}
<!-- Libelle Field -->
<div class="form-group">
    {!! Form::label('libelle', 'Libelle:') !!}
    <p>{!! $promo->libelle_promo !!}</p>
</div>

<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $promo->description !!}</p>
</div>

<div class="form-group">
    {!! Form::label('date_debut', 'Date de debut:') !!}
    <?php $date= strtotime($promo->date_debut) ?>
    <p>{!! $dateRelise= date("d/m/y",$date) !!}</p>
</div>
<div class="form-group">
    {!! Form::label('date_fin', 'Date de fin:') !!}
    <?php $date= strtotime($promo->date_fin) ?>
    <p>{!! $dateRelise= date("d/m/y",$date) !!}</p>
</div>

<table class="table table-responsive" id="categories-table">
    <thead>
        <tr>
            <th>Libelle</th>
            <th>Desctiption</th>
            <th>Date de debut</th>
            <th>Date de fin</th>
            <th class="text-center">Etat</th>
            <th class="text-center" colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($promos as $promo)
        <tr>
            <td>{!! $promo->libelle_promo !!}</td>
            <td>{!! $promo->description !!}</td>
            <td><?php $date= strtotime($promo->date_debut) ?>
                {!! $dateRelise= date("d/m/y",$date) !!}</td>
            <td>
                <?php $date= strtotime($promo->date_fin) ?>
                {!! $dateRelise= date("d/m/y",$date)!!}</td>

                @if ( date_create(date_default_timezone_get())>$promo->date_debut && date_create(date_default_timezone_get())<=$promo->date_fin )
                            @if(date_diff($promo->date_fin, date_create(date_default_timezone_get()))->days < 1)
                            <td class="text-center">
                                <span class="label label-danger">En cours (0)jrs</span>
                            </td>
                        @elseif(date_diff($promo->date_fin,date_create(date_default_timezone_get()))->days < 2)
                            <td class="text-center">
                                <span class="label label-warning">En cours ({{ date_diff($promo->date_fin,date_create(date_default_timezone_get()))->days }}) jrs</span>
                            </td>
                        @else
                            <td class="text-center">
                                <span class="label label-success">En cours ({{ date_diff($promo->date_fin,date_create(date_default_timezone_get()))->days }}) jrs</span>
                            </td>
                        @endif
                @elseif(date_create(date_default_timezone_get())>$promo->date_fin)   
                    <td class="text-center">
                        <span class="label label-info">Cloturée</span>
                    </td>         
                @else     
                <td class="text-center">
                        <span class="label label-default">Non debutée</span>
                    </td>       
                @endif

            <td class="text-center">
                {!! Form::open(['route' => ['admin.promos.destroy', $promo->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.promos.show', [$promo->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.promos.edit', [$promo->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
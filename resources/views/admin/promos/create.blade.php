@extends('layouts.adminApp')

@section('content')
    <section class="content-header">
        <h1>
            Promotion
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'admin.promos.store']) !!}
                    {!! csrf_field() !!}
                    @include('admin.promos.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function () {
            $('.flatpickr').flatpickr();
        })
    </script>
@endsection

<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $typeAbonnement->id !!}</p>
</div>

<!-- Libelle Field -->
<div class="form-group">
    {!! Form::label('libelle', 'Libelle:') !!}
    <p>{!! $typeAbonnement->libelle !!}</p>
</div>

<!-- Mt Mensuel Field -->
<div class="form-group">
    {!! Form::label('mt_mensuel', 'Mt Mensuel:') !!}
    <p>{!! $typeAbonnement->mt_mensuel !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $typeAbonnement->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $typeAbonnement->updated_at !!}</p>
</div>


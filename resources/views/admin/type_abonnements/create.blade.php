@extends('layouts.adminApp')

@section('content')
    <section class="content-header">
        <h1>
            Type Abonnement
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'admin.typeAbonnements.store']) !!}

                        @include('admin.type_abonnements.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

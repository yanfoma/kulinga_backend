<!-- Libelle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('libelle', 'Libelle:') !!}
    {!! Form::text('libelle', null, ['class' => 'form-control']) !!}
</div>

<!-- Mt Mensuel Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mt_mensuel', 'Mt Mensuel (CFA) :') !!}
    {!! Form::number('mt_mensuel', null, ['class' => 'form-control']) !!}
</div>

<!-- Mt Mensuel Field -->
<div class="form-group col-sm-6">
    {!! Form::label('taux_echange', 'Taux de change (USD) :') !!}
    {!! Form::number('taux_echange', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.typeAbonnements.index') !!}" class="btn btn-default">Cancel</a>
</div>

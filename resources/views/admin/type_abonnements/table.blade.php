<table class="table table-responsive" id="typeAbonnements-table">
    <thead>
    <tr>
        <th>Libelle</th>
        <th>Mt Mensuel <span>CFA</span></th>
        <th>Taux de change <span>USD</span></th>
        <th colspan="3">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($typeAbonnements as $typeAbonnement)
        <tr>
            <td>{!! $typeAbonnement->libelle !!}</td>
            <td>{!! $typeAbonnement->mt_mensuel !!}</td>
            <td>{!! $typeAbonnement->taux_echange !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.typeAbonnements.destroy', $typeAbonnement->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.typeAbonnements.show', [$typeAbonnement->id]) !!}"
                       class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.typeAbonnements.edit', [$typeAbonnement->id]) !!}"
                       class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@extends('layouts.adminApp')

@section('content')
    <section class="content-header">
        <h1>
            Auteur
        </h1>
   </section>
   <div class="content">
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($auteur, ['route' => ['admin.auteurs.update', $auteur->id], 'method' => 'patch']) !!}

                        @include('admin.auteurs.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
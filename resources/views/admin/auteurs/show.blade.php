@extends('layouts.adminApp')

@section('content')
    <section class="content-header">
        <h1>
            Auteur
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('admin.auteurs.show_fields')
                    <a href="{!! route('admin.auteurs.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection

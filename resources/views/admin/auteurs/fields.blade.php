<!-- Nom Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Identité de l\'auteur', 'Nom:') !!}
    {!! Form::text('nom', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6 has-feedback{{ $errors->has('pays') ? ' has-error' : '' }}">
    <label>Pays de l'auteur</label>
    <select class="form-control" name="pays" id="pays">

        @foreach((new Monarobase\CountryList\CountryList)->getList('fr') as $pays)
           
            <option value='{{$pays}}'
                @if ($auteur != null and $pays==old('pays', $auteur->pays))
                    selected="selected"
                @endif
            >
            {{$pays}}</option>
            
        @endforeach
    </select>

    @if ($errors->has('pays'))
        <span class="help-block">
            <strong>{{ $errors->first('pays') }}</strong>
        </span>
    @endif
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.auteurs.index') !!}" class="btn btn-default">Cancel</a>
</div>

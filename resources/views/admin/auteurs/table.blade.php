<table class="table table-responsive" id="auteurs-table">
    <thead>
    <tr>
        <th>Nom</th>
        <th>Pays de l'auteur</th>
        <th colspan="3">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($auteurs as $auteur)
        <tr>
            <td>{!! $auteur->nom !!}</td>
            <td>{!! $auteur->pays !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.auteurs.destroy', $auteur->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.auteurs.show', [$auteur->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.auteurs.edit', [$auteur->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
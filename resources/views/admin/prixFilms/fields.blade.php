<!-- Libelle Field -->
 
<div class="form-group col-sm-6">
    {!! Form::label('titre_prix', 'Titre:') !!}
    {!! Form::text('titre_prix', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('description_prix', 'Description:') !!}
    {!! Form::text('description_prix', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('valeur_prix', 'Prix:') !!}
    {!! Form::text('valeur_prix', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.prixFilms.index',[$film->id]) !!}" class="btn btn-default">Cancel</a>
</div>

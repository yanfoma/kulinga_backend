@extends('layouts.adminApp')

@section('content')
    <section class="content-header">
        
        <h1 class="pull-left">Prix obtenu: </h1>
        <h1 class="pull-left"><a href="{!! route('admin.films.show', [$film->id]) !!}">{{$film->titre}}</a></h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('admin.prixFilms.create', [$film->id]) !!}">Add New</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('admin.prixFilms.table')
            </div>
        </div>
    </div>
@endsection


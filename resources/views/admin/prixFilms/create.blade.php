@extends('layouts.adminApp')

@section('content')
    <section class="content-header">
        <h1>
            Prix filme:{{$film->titre}}
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                 {!! Form::open(['route' => 'admin.prixFilms.store']) !!}

                        @include('admin.prixFilms.fields')
                <input type="hidden" value="{{$film->id}}" name="film_id" id="film_id">

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.adminApp')

@section('content')
    <section class="content-header">
        <h1>
            Prix film:{{$film->titre}}
        </h1>
   </section>
   <div class="content">
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($prixFilm, ['route' => ['admin.prixFilms.update', $prixFilm->id], 'method' => 'patch']) !!}

                        @include('admin.prixFilms.fields')
                        {{-- <input type="hidden" value="{{$film->id}}" name="film_id" id="film_id"> --}}

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
@extends('layouts.adminApp')

@section('content')
    <section class="content-header">
        <h1>
            Prix film
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('admin.prixFilms.show_fields')
                    <a href="{!! route('admin.prixFilms.index',[$film->id]) !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection

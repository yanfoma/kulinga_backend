<table class="table table-responsive" id="categories-table">
    <thead>
        <tr>
            <th>Titre</th>
            <th>Description</th>
            <th>Valeur</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($prixFilms as $prixFilm)
    <tr>
        <td>{!! $prixFilm->titre_prix !!}</td>
        <td>{!! $prixFilm->description_prix !!}</td>
        <td>{!! $prixFilm->valeur_prix !!}</td>
        <td>
            {!! Form::open(['route' => ['admin.prixFilms.destroy', $prixFilm->id], 'method' => 'delete']) !!}
            <div class='btn-group'>
                <a href="{!! route('admin.prixFilms.show', [$prixFilm->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                <a href="{!! route('admin.prixFilms.edit', [$prixFilm->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
            </div>
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </tbody>
</table>
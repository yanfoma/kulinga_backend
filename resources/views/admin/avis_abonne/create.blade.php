@extends('layouts.adminApp')

@section('content')
    <section class="content-header">
        <h1>
            Avis des abonné
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'avis_abonne.store']) !!}

                    @include('admin.avis_abonne.fields')

                    {!! Form::close() !!} 
                </div>
            </div>
        </div>
    </div>
@endsection

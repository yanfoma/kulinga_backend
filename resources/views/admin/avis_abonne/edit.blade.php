@extends('layouts.adminApp')

@section('content')
    <section class="content-header">
        <h1>
            Modification de l'avis
        </h1>
   </section>
   <div class="content">
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">

                   {!! Form::model($avis_abonne, ['route' => ['avis_abonne.update', $avis_abonne->id], 'method' => 'patch']) !!}

                        @include('admin.avis_abonne.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
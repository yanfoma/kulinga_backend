@extends('layouts.adminApp')

@section('content')
    <section class="content-header">
        @if(Auth::user()->hasAnyRole('subscriber'))
            <h1 class="pull-left">Vos sommentaires</h1>
        @else
            <h1 class="pull-left">Commentaires des abonnés</h1>
        @endif
        <h1 class="pull-right">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px"
               href="{!! route('avis_abonne.create') !!}">Add New</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                @include('admin.avis_abonne.table')
            </div>
        </div>
        <div class="text-center">
            {{$avis_abonnes->links()}}
        </div>
    </div>
@endsection


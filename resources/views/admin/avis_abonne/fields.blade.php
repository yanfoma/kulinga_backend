<!-- titre Field -->
<div class="form-group col-xs-12 col-sm-6 col-lg-6">
    {!! Form::label('titre', 'Objet de l\'avis:') !!}
    {!! Form::text('titre', null, ['class' => 'form-control']) !!}
</div>

<!-- message Field -->
<div class="col-sm-12 col-xs-12 col-lg-12">
    {!! Form::label('message', 'Votre message:') !!}
</div>
<div class="form-group col-sm-12 col-xs-12">
    {!! Form::textarea('message', null, ['id' => 'message', 'rows' => 10, 'cols'=>82, 'style' => 'resize:none; width:auto']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('avis_abonne.index') !!}" class="btn btn-default">Cancel</a>
</div>

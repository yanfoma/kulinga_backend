@extends('layouts.adminApp')

@section('content')
    <section class="content-header">
        <h1>
            Types film
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'type_film.store']) !!}

                        @include('admin.type_film.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

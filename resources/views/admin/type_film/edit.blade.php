@extends('layouts.adminApp')

@section('content')
    <section class="content-header">
        <h1>
            Edit type film
        </h1>
   </section>
   <div class="content">
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($typeFilm, ['route' => ['type_film.update', $typeFilm->id], 'method' => 'patch']) !!}

                        @include('admin.type_film.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
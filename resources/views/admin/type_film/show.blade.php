@extends('layouts.adminApp')

@section('content')
    <section class="content-header">
        <h1>
            Detail Type Film
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('admin.type_film.show_fields')
                    <a href="{!! route('type_film.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection

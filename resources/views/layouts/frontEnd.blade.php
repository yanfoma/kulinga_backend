<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.head')
    @yield('styles')
</head>
<body class="page">
<div class="page__layout">
    <div class="overlay"></div>
    @include('partials.header')
    <div id="content-ajax" style="margin-top: 60px">
        <main class="page__main main">
            <div class="inner inner--withslider">
             
                @yield('content')
            </div>
        </main>
     
    </div>
</div>
@include('partials.scripts')
@yield('scripts')
</body>
</html>
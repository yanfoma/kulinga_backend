<li class="{{ Request::is('films*') ? 'active' : '' }}">
    <a href="{!! route('admin.films.index') !!}"><i class="fa fa-edit"></i><span>Films</span></a>
</li>

<li class="{{ Request::is('Promos*') ? 'active' : '' }}">
    <a href="{!! route('admin.promos.index') !!}"><i class="fa fa-edit"></i><span>Promos</span></a> 
</li>

<li class="{{ Request::is('typefilms*') ? 'active' : '' }}">
    <a href="{!! route('type_film.index') !!}"><i class="fa fa-edit"></i><span>Types de film</span></a>
</li>

<li class="{{ Request::is('auteurs*') ? 'active' : '' }}">
    <a href="{!! route('admin.auteurs.index') !!}"><i class="fa fa-edit"></i><span>Auteurs</span></a>
</li>

<li class="{{ Request::is('abonnements*') ? 'active' : '' }}">
    <a href="{!! route('admin.abonnements.index') !!}"><i class="fa fa-edit"></i><span>Abonnements</span></a>
</li>

<li class="{{ Request::is('typeAbonnements*') ? 'active' : '' }}">
    <a href="{!! route('admin.typeAbonnements.index') !!}"><i class="fa fa-edit"></i><span>Types d'abonnement</span></a>
</li>

<li class="{{ Request::is('categories*') ? 'active' : '' }}">
    <a href="{!! route('admin.categories.index') !!}"><i class="fa fa-edit"></i><span>Categories</span></a>
</li>

<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('admin.users.index') !!}"><i class="fa fa-edit"></i><span>Users</span></a>
</li>

<li class="{{ Request::is('avisAbonnes*') ? 'active' : '' }}">
    <a href="{!! route('avis_abonne.index') !!}"><i class="fa fa-edit"></i><span>Avis des abonnés</span></a>
</li>


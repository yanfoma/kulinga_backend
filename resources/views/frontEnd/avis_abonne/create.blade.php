@extends('frontEnd.Abonnnements.abonneApp')

@section('content')
    <section class="content-header">
        <h1>
            En quoi pouvons-nous vous aider ?
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'avis_abonne.store', 'id'=>'avis_create_form_id']) !!}

                    @include('frontEnd.avis_abonne.fields')

                    {!! Form::close() !!}
                </div>
                <hr>
                    <div class="col-lg-12">
                        <h1>Vos commentaires</h1>
                        <hr>
                        <div class="col-lg-12">
                            <div class="pre-scrollable">
                                @if (!empty($avis_abonnes))
                                    @foreach ($avis_abonnes as $avis_abonne)
                                        <p>{{$avis_abonne->message}}</p>
                                        <span class="text-muted">3 months ago</span>
                                        <hr>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="text-center">
                                {{$avis_abonnes->links()}}
                            </div>
                    </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function() {
            $('#avis_create_form_id').on('submit', function(event) {
                event.preventDefault();
                var _this = $(this);

                $.ajax({
                    url: _this.attr('action'),
                    type: 'post',
                    dataType: 'text',
                    data: _this.serialize()
                }).done(function(data) {
                    if(data === 'success') {
                        location.reload(true);
                    } else {
                        
                    }
                });
            })
        });
    </script>
@endsection

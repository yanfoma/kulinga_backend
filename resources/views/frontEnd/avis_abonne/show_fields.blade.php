<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $avis_abonne->id !!}</p>
</div>

<!-- Libelle Field -->
<div class="form-group">
    {!! Form::label('titre', 'Objet du commentaire:') !!}
    <p>{!! $avis_abonne->titre !!}</p>
</div>

<!-- Libelle Field -->
<div class="form-group">
    {!! Form::label('message', 'Le message soumis:') !!}
    <p>{!! $avis_abonne->message !!}</p>
</div>
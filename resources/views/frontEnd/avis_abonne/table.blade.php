<table class="table table-responsive" id="typeFilms-table">
    <thead>
    <tr>
        <th>Objet</th>
        <th>Message</th>
        <th colspan="3">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($avis_abonnes as $avis_abonne)
        <tr>
            <td>{!! $avis_abonne->titre !!}</td>
            <td>{!! $avis_abonne->message !!}</td>
            <td>
                {!! Form::open(['route' => ['avis_abonne.destroy', $avis_abonne->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('avis_abonne.show', [$avis_abonne->id]) !!}"
                       class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('avis_abonne.edit', [$avis_abonne->id]) !!}"
                       class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
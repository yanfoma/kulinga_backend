@extends('frontEnd.Abonnnements.abonneApp')

@section('content')
    <section class="content-header">
        <h1>
            Détails du commentaire
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('frontEnd.avis_abonne.show_fields')
                    <a href="{!! route('avis_abonne.index') !!}" class="btn btn-default">Retour</a>
                </div>
            </div>
        </div>
    </div>
@endsection

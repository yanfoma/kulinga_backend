@extends('frontEnd.Abonnnements.abonneApp')
@section('css')
    <style>
        #table_section {
            height: 100vh;
            border-left: solid 1px blue;
        }
        .alert {
            margin: 0;
            padding: 5px;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <aside class="col-sm-3">
            <div class="box-primary">
                <h4>Renouveller votre abonnement</h4>
                <div class="box-body">
                    <div class="row">
                    {!! Form::open(['route' => 'espace.abonne.renouveller', 'id' => 'new_form_id']) !!}

                    {!! csrf_field() !!}

                    <!-- Type abonnement -->
                        <div class="form-group has-feedback{{ $errors->has('type_abonnement') ? ' has-error' : '' }}">
                            <label>Type d'abonnement :</label>
                            <select class="form-control" name="type_abonnement" id="type_abonnement" required>
                                @foreach($type_abonnements as $type_abonnement)
                                    <option id='{{$type_abonnement->mt_mensuel}}'
                                            value='{{$type_abonnement->id}}'>{{$type_abonnement->libelle}}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('type_abonnement'))
                                <span class="help-block">
                                 <strong>{{ $errors->first('type_abonnement') }}</strong>
                            </span>
                            @endif
                        </div>

                        <!-- nb_mois Field -->
                        <div class="form-group has-feedback{{ $errors->has('nombre_mois')  ? ' has-error' : '' }}">
                            {!! Form::label('nombre_mois', 'Nombre de mois :') !!}
                            {!! Form::number('nombre_mois', null,
                                ['class' => 'form-control', 'required' => 'required', 'min' => 0, 'id' => 'nb_mois', 'value' => old('nombre_mois'), 'placeholder' => 'Nombre de moi d\'abonnement'])
                            !!}
                            @if ($errors->has('nombre_mois'))
                                <span class="help-block">
                                <strong>{{ $errors->first('nombre_mois') }}</strong>
                            </span>
                            @endif
                        </div>

                        <!-- Montn -->

                        <div class="inpu-group form-group form-inline has-feedback">
                            <label class="col-sm-7">Montant mensuel</label>
                            <label class="col-sm-3 text-center" id="mnt_mois">0</label>
                            <div class="input-group-append col-sm-2 pull-right">
                                <span class="input-group-text">FCFA</span>
                            </div>
                        </div>

                        &nbsp;

                        <div class="inpu-group form-group form-inline has-feedback">
                            <label class="col-sm-7">Montant total</label>
                            <label class="col-sm-3 text-center" id="mnt_total">0</label>
                            <div class="input-group-append col-sm-2 pull-right">
                                <span class="input-group-text">FCFA</span>
                            </div>
                        </div>

                        &nbsp;
                        <!-- Submit Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::submit('Valider', ['class' => 'btn btn-primary', 'disabled' => ($can_subscribe == 'cannot')]) !!}
                            {!! Form::reset('Annuler', ['class' => 'btn btn-default']) !!}
                        </div>


                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </aside>
        <section class="col-sm-9 table-responsive panel" id="table_section">
            <h1>Historique de vos abonnements</h1>
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>N°</th>
                    <th>Date abonnement</th>
                    <th>Nombre de mois</th>
                    <th>Type abonnement</th>
                    <th>Montant de l'abonnement</th>
                    <th>Jours restant</th>
                </tr>
                </thead>
                <tbody>
                @foreach($abonnements as $abonnement)
                    <tr>
                        <td class="text-center">{{$loop->index+1}}</td>
                        <td class="text-center">{{date('d/m/y - H:i', strtotime($abonnement->created_at))}}</td>
                        <td class="text-center">{{$abonnement->nb_mois}}</td>
                        @foreach($type_abonnements as $type_abonnement)
                            @if($type_abonnement->id == $abonnement->typeabonnement_id)
                                <td class="text-center">{{$type_abonnement->libelle}}</td>
                                <td class="text-center">{{$abonnement->nb_mois * $type_abonnement->mt_mensuel}}</td>
                            @endif
                        @endforeach
                        @if(date_diff(date_create(date_default_timezone_get()), date_create($abonnement->created_at))->m > $abonnement->nb_mois)
                            <td class="text-center">
                                <span class="fa fa-cart-arrow-down alert alert-danger"> (0) jrs</span>
                            </td>
                        @elseif(($abonnement->nb_mois - date_diff(date_create($abonnement->created_at), date_create(date_default_timezone_get()))->m) < 2)
                            <td class="text-center">
                                <span class="fa fa-cart-arrow-down alert alert-warning"> ({{$abonnement->nb_mois*30 - date_diff(date_create($abonnement->created_at), date_create(date_default_timezone_get()))->days}}
                                    ) jrs</span>
                            </td>
                        @else
                            <td class="text-center">
                                <span class="fa fa-cart-arrow-down alert alert-success"> ({{$abonnement->nb_mois*30 - date_diff(date_create($abonnement->created_at), date_create(date_default_timezone_get()))->days}}
                                    ) jrs</span>
                            </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </section>
    </div>
@endsection
@section('scripts')
    <script>
        $(function () {
            var pays = $('#pays');
            var type_abonnement = $('#type_abonnement');

            {
                pays.select2({
                    theme: 'classic'
                });
                type_abonnement.select2({
                    theme: 'classic',
                    placeholder: 'Choisir votre type d\'abonnement'
                });
            }

            var mnt_mensuel;

            {
                mnt_mensuel = type_abonnement.children(':selected').attr('id');
                $('#mnt_mois').html(parseFloat(mnt_mensuel));
            }

            type_abonnement.on('change', function () {
                var _this = $(this);
                mnt_mensuel = _this.children(':selected').attr('id');

                $('#mnt_mois').html(parseFloat(mnt_mensuel));

                var nb_mois = $('#nb_mois').val();

                if (nb_mois !== 0 || nb_mois !== null) {
                    $('#mnt_total').html(parseFloat(nb_mois) * mnt_mensuel);
                }
            });

            $('#nb_mois').on('keyup', function () {
                var _this = $(this);

                var value = _this.val();
                if (value !== 0 || value !== '') {
                    $('#mnt_total').html(parseFloat(value) * mnt_mensuel);
                } else {
                    console.log('Vous devez choisir un type abonnement !')
                }
            });

            /**
             * Gestion de la soumission du formulaire
             */
            $('#new_form_id').on('submit', function (event) {
                event.preventDefault();

                var _this = $(this);

                $.ajax({
                    type: 'POST',
                    url: _this.attr('action'),
                    data: _this.serialize(),
                    dataType: 'text'
                }).done(function (data) {
                    if (data === 'ok') {
                        location.reload(true);
                    }
                });

                return false
            });
        });
    </script>
@endsection
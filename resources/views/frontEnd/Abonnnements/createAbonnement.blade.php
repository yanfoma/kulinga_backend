@extends('frontEnd.Abonnnements.abonneApp')
@section('css')
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Création d'un compte abonné
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">

            <div class="box-body">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                {!! Form::open(['route' => 'espace.abonne.store']) !!}

                {!! csrf_field() !!}

                <!-- Nom Field -->
                    <div class="form-group col-sm-8 col-xs-12 has-feedback{{ $errors->has('nom') ? ' has-error' : '' }}">
                        {!! Form::label('nom', 'Nom :') !!}
                        {!! Form::text('nom', null,
                            ['class' => 'form-control', 'required' => 'required', 'value' => old('nom'), 'placeholder' => 'Nom de famille'])
                        !!}
                        @if ($errors->has('nom'))
                            <span class="help-block">
                                <strong>{{ $errors->first('nom') }}</strong>
                            </span>
                        @endif
                    </div>

                    <!-- Prenom Field -->
                    <div class="form-group col-sm-8 col-xs-12 has-feedback{{ $errors->has('prenom') ? ' has-error' : '' }}">
                        {!! Form::label('prenom', 'Prénom :') !!}
                        {!! Form::text('prenom', null,
                            ['class' => 'form-control', 'required' => 'required', 'value' => old('prenom'), 'placeholder' => 'Prénom'])
                        !!}

                        @if ($errors->has('prenom'))
                            <span class="help-block">
                                <strong>{{ $errors->first('prenom') }}</strong>
                            </span>
                        @endif
                    </div>

                    <!-- Telephone Field -->
                    <div class="form-group col-sm-8 col-xs-12 has-feedback{{ $errors->has('telephone')  ? ' has-error' : '' }}">
                        {!! Form::label('telephone', 'Téléphone :') !!}
                        {!! Form::text('telephone', null,
                            ['class' => 'form-control', 'required' => 'required', 'value' => old('telephone'), 'placeholder' => 'Téléphone'])
                        !!}
                        @if ($errors->has('telephone'))
                            <span class="help-block">
                                <strong>{{ $errors->first('telephone') }}</strong>
                            </span>
                        @endif
                    </div>

                    <!-- Pays user -->
                    <div class="form-group col-sm-8 col-xs-12 has-feedback{{ $errors->has('pays') ? ' has-error' : '' }}">
                        <label>Pays de l'abonné :</label>
                        <select class="form-control" name="pays" id="pays" required>
                            @foreach((new Monarobase\CountryList\CountryList)->getList('fr') as $pays)
                                <option value='{{$pays}}'>{{$pays}}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('pays'))
                            <span class="help-block">
                                 <strong>{{ $errors->first('pays') }}</strong>
                            </span>
                        @endif
                    </div>
                    <!-- Login Field -->
                    <div class="form-group col-sm-8 col-xs-12 has-feedback{{ $errors->has('login') ? ' has-error' : '' }}">
                        {!! Form::label('login', 'Nom d\'utilisateur :') !!}
                        {!! Form::text('login', null,
                            ['class' => 'form-control', 'required' => 'required', 'value' => old('login'), 'placeholder' => 'Nom d\'utilisateur'])
                        !!}

                        @if ($errors->has('login'))
                            <span class="help-block">
                                <strong>{{ $errors->first('login') }}</strong>
                            </span>
                        @endif
                    </div>

                    <!-- Email Field -->
                    <div class="form-group col-sm-8 col-xs-12 has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('email', 'Email :') !!}
                        {!! Form::email('email', null,
                            ['class' => 'form-control', 'required' => 'required', 'value' => old('email'), 'placeholder' => 'Email'])
                        !!}

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <!-- Password Field -->
                    <div class="form-group col-sm-8 col-xs-12 has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                        {!! Form::label('password', 'Mot de passe :') !!}
                        {!! Form::password('password',
                            ['class' => 'form-control', 'required' => 'required', 'value' => old('password'), 'placeholder' => 'Mot de passe'])
                        !!}

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                    <!-- Password Field -->
                    <div class="form-group col-sm-8 col-xs-12 has-feedback{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        {!! Form::label('password_confirmation', 'Repéter le mot de passe :') !!}
                        {!! Form::password('password_confirmation',
                            ['class' => 'form-control', 'required' => 'required', 'value' => old('password_confirmation'), 'placeholder' => 'Repéter le mot de passe'])
                        !!}

                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-sm-12 col-xs-12">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        <a href="{!! url('/') !!}" class="btn btn-default">Cancel</a>
                    </div>


                    {!! Form::close() !!}
                </div>
                <div class="col-sm-2"></div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function () {
            var pays = $('#pays');
            var type_abonnement = $('#type_abonnement');

            {
                pays.select2({
                    theme: 'classic'
                });
                /*type_abonnement.select2({
                    theme: 'classic',
                    placeholder: 'Choisir votre type d\'abonnement'
                });*/
            }

            /*var mnt_mensuel;

            {
                mnt_mensuel = type_abonnement.children(':selected').attr('id');
                $('#mnt_mois').html(parseFloat(mnt_mensuel));
            }

            type_abonnement.on('change', function () {
                var _this = $(this);
                mnt_mensuel = _this.children(':selected').attr('id');

                $('#mnt_mois').html(parseFloat(mnt_mensuel));

                var nb_mois = $('#nb_mois').val();

                if (nb_mois !== 0 || nb_mois !== null) {
                    $('#mnt_total').html(parseFloat(nb_mois) * mnt_mensuel);
                }
            });

            $('#nb_mois').on('keyup', function () {
                var _this = $(this);

                var value = _this.val();
                if (value !== 0 || value !== '') {
                    $('#mnt_total').html(parseFloat(value) * mnt_mensuel);
                } else {
                    console.log('Vous devez choisir un type abonnement !')
                }
            })*/
        });
    </script>
@endsection
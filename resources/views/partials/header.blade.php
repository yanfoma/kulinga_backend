<!--HEADER LOGIN-->
<header class="page__header header header_2 " style="margin-bottom: 15px">
    <div class="container-fluid">
        <div class="header__left">
            <a href="{!! url('/') !!}" class="logo">
                <img src="{!!asset('images/logokulinga.jpeg')!!}" height="30" width="200" alt=""> </a>
            <nav class="header__nav">
                <ul class="header__nav-list">
                @if (\Illuminate\Support\Facades\Auth::user())
                    <li class="header__nav-item header__nav-item--dropdown">
                        <span class="header__nav-link mobile-ajax-off">Categories </span>
                        <div class="header__nav-dropdown header__nav-dropdown--categories">
                          <ul> 
                            @foreach ($categories as $categorie)
                                    <li>
                                        <a href="{{ route('menus.show', $categorie->id) }}"
                                           class=""> {{ $categorie->libelle }} </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </li>
                 @endif
                </ul>
            </nav>
        </div>
        <div class="header__right">
            <div class="btn-group user-login">
                @if(\Illuminate\Support\Facades\Auth::check())
                    @if(\Illuminate\Support\Facades\Auth::user()->hasRole('subscriber'))
                        <a href="{{route('espace.abonne.home')}}" class="btn-group__first">Espace abonné</a>
                        &nbsp;
                    @endif
                    <div class="pull-right">
                        <a href="{!! url('/logout') !!}" class=""
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <span class="text-danger">Se déconnecter</span>
                        </a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                @else
                    <a href="{{route('espace.abonne.create')}}" class="btn-group__first">Inscrivez-vous</a>
                    <a href="{{route('login')}}" class="btn-group__first">Se connecter</a>
                @endif
            </div>
            <a href="#" class="search-open"></a>
            <a href="#" class="search-close"></a>
        </div>
        <!-- search form -->
        <form action="#" class="search">
            <input type="text" name="search" class="search__field" placeholder="Search cutetube.com" autocomplete="on">
            <button class="search__button" type="submit"></button>
            <div class="search__enter">Taper sur Enter</div>
        </form>
        <!-- /search form -->
        <a href="#" class="menu-open">
            <span></span>
            <span></span>
            <span></span>
        </a>
        <a href="#" class="menu-close">
            <span></span>
            <span></span>
            <span></span>
        </a>
    </div>
    <div class="mobile-menu">
        <div class="mobile-menu__scroll-content"></div>
    </div>
</header>
<!--/HEADER LOGIN-->
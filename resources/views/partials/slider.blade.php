<div class="hero-slider-wrap scrollreveal scrollAnimateHeroSlider">
    <div class="hero-slider">
        <div class="sld">
            <div class="hero-slider__slide">
                <div class="hero-slider__slide-image">
                    <span class="hero-slider__lazy-load-img" data-original="images/slide_01.jpg"></span>
                </div>
                <div class="container">
                    <div class="row row--flex">
                        <div class="col-sm-12">
                            <div class="hero-slider__slide-left">
                                <div class="hero-slider__slide-text text-center">
                                    <span  class="hero-slider__slide-name js-ajax-link text-danger">
                                        <h2> Waiting for your feedbacks, important to improve your user experience ! </h2>
                                    </span>
                                    @if (!is_null($promo))
                                    <div class="hero-slider__button">
                                    <a href="{{route('menus.index')}}" class="btn btn--play pull-right"> <h1>WATCH FREE FOR {{ $nbJr}} DAYS<span class="glyphicon glyphicon-chevron-right"></span> </h1></a>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
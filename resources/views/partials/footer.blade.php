<footer class="footer page__footer footer scrollreveal scrollAnimateFade">
    <div class="footer__inner">
        <div class="footer__top">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="footer__top-heading"> Information </div>
                        <ul class="footer__top-list">
                            <li>
                                <a href="#" class="">Confidentialité</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <div class="footer__top-heading"> Assistance </div>
                        <ul class="footer__top-list">
                            <li>
                                <a href="#" class="">FAQ</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <div class="footer__top-heading"> Contactez nous </div>
                        <ul class="footer__top-list">
                            <li>
                                <a href="{!! route('avis_abonne.create') !!}">Contact Us</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        {{--<div class="footer__social">
            <div class="container">
                <div class="row">
                    <div class="column">
                        <a href="#" class="footer__social-item">
                            <img src="images/reddit_logo.svg" alt="reddit">
                            <div class="footer__social-info">
                                <div class="footer__social-subscribers"> 12 622 </div>
                                <div class="footer__social-username"> @cutetube </div>
                            </div>
                        </a>
                    </div>
                    <div class="column">
                        <a href="#" class="footer__social-item">
                            <img src="images/twitter_logo.svg" alt="tumblr">
                            <div class="footer__social-info">
                                <div class="footer__social-subscribers"> 27 332 </div>
                                <div class="footer__social-username"> @cutetube </div>
                            </div>
                        </a>
                    </div>
                    <div class="column">
                        <a href="#" class="footer__social-item">
                            <img src="images/googleplus_logo.svg" alt="googleplus">
                            <div class="footer__social-info">
                                <div class="footer__social-subscribers"> 31 478 </div>
                                <div class="footer__social-username"> @cutetube </div>
                            </div>
                        </a>
                    </div>
                </div>--}}
            </div>
        </div>
        <!--FOOTER SMALL-->
        <div class="footer__bottom">
            <div class="container">
                <div class="row">
                    <div class="column">
                        <strong>Copyright © 2019 <a href="http://yanfoma.tech" target="_blank">Yanfoma</a>.</strong> All rights
                        reserved.
                    </div>
                    <div class="column footer__bottom-created-by">
                        <i>Created by</i> &nbsp;
                        <img src="{!! asset('images/logokulinga.jpeg') !!}" alt="" width="110" height="40">
                    </div>
                </div>
            </div>
        </div>
        <!--/FOOTER SMALL-->
    </div>
</footer>
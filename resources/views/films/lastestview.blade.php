<div class="section videos-section scrollreveal scrollAnimateFade">
    <div class="container">
        <div class="section__inner">
            <div class="section-heading">
                <h4>Latest</h4>
                <a href="#" class="section-heading__control section-heading__see-all"> See all </a>
            </div>
            <div class="row row--flex">
                <div class="col-lg-4 col-sm-6">
                    <a href="./video.html" class="video-preview video-preview--md js-ajax-link">
                        <div class="video-preview__image">
                            <span class="lazy-bg-img" data-original="images/photo_05.jpg"></span>
                            <div class="video-preview__info">
                                <div class="video-preview__duration">12:04</div>
                                <div class="video-preview__likes">88%</div>
                                <div class="video-preview__quality">HD</div>
                            </div>
                        </div>
                        <h5 class="video-preview__descr">People rejoice at the Sun, and I’m dreaming of the Moon</h5>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="./video.html" class="video-preview video-preview--md js-ajax-link">
                        <div class="video-preview__image">
                            <span class="lazy-bg-img" data-original="images/photo_07.jpg"></span>
                            <div class="video-preview__info">
                                <div class="video-preview__duration">12:04</div>
                                <div class="video-preview__likes">88%</div>
                                <div class="video-preview__quality">HD</div>
                            </div>
                        </div>
                        <h5 class="video-preview__descr">One word frees us of all the weight and pain of life: that word is love</h5>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="./video.html" class="video-preview video-preview--md js-ajax-link">
                        <div class="video-preview__image">
                            <span class="lazy-bg-img" data-original="images/photo_08.jpg"></span>
                            <div class="video-preview__info">
                                <div class="video-preview__duration">12:04</div>
                                <div class="video-preview__likes">88%</div>
                                <div class="video-preview__quality">HD</div>
                            </div>
                        </div>
                        <h5 class="video-preview__descr">Always forgive your enemies — nothing annoys them so much</h5>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="./video.html" class="video-preview video-preview--md js-ajax-link">
                        <div class="video-preview__image">
                            <span class="lazy-bg-img" data-original="images/photo_09.jpg"></span>
                            <div class="video-preview__info">
                                <div class="video-preview__duration">12:04</div>
                                <div class="video-preview__likes">88%</div>
                                <div class="video-preview__quality">HD</div>
                            </div>
                        </div>
                        <h5 class="video-preview__descr">He, who does not love loneliness, does not love freedom</h5>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="./video.html" class="video-preview video-preview--md js-ajax-link">
                        <div class="video-preview__image">
                            <span class="lazy-bg-img" data-original="images/photo_10.jpg"></span>
                            <div class="video-preview__info">
                                <div class="video-preview__duration">12:04</div>
                                <div class="video-preview__likes">88%</div>
                                <div class="video-preview__quality">HD</div>
                            </div>
                        </div>
                        <h5 class="video-preview__descr">A dream becomes a goal when action is taken toward its achievement</h5>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="./video.html" class="video-preview video-preview--md js-ajax-link">
                        <div class="video-preview__image">
                            <span class="lazy-bg-img" data-original="images/photo_11.jpg"></span>
                            <div class="video-preview__info">
                                <div class="video-preview__duration">12:04</div>
                                <div class="video-preview__likes">88%</div>
                                <div class="video-preview__quality">HD</div>
                            </div>
                        </div>
                        <h5 class="video-preview__descr">People rejoice at the Sun, and I’m dreaming of the Moon</h5>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

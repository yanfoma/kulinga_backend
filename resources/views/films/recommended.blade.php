<div class="section videos-section scrollreveal scrollAnimateFade">
    <div class="container">
        <div class="section__inner">
            <div class="section-heading">
                <h4>Recommended for you</h4>
                <a href="#" class="section-heading__control section-heading__see-all"> See all </a>
            </div>
            <div class="row row--flex">
                <div class="col-xl-3 col-lg-4 col-sm-6">
                    <a href="./video.html" class="video-preview video-preview--md js-ajax-link">
                        <div class="video-preview__image">
                            <span class="lazy-bg-img" data-original="images/photo_12.jpg"></span>
                            <div class="video-preview__info">
                                <div class="video-preview__duration">12:04</div>
                                <div class="video-preview__likes">88%</div>
                                <div class="video-preview__quality">HD</div>
                            </div>
                        </div>
                        <h5 class="video-preview__descr">If you want to be somebody, somebody really special, be yourself!</h5>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-4 col-sm-6">
                    <a href="./video.html" class="video-preview video-preview--md js-ajax-link">
                        <div class="video-preview__image">
                            <span class="lazy-bg-img" data-original="images/photo_13.jpg"></span>
                            <div class="video-preview__info">
                                <div class="video-preview__duration">12:04</div>
                                <div class="video-preview__likes">88%</div>
                                <div class="video-preview__quality">HD</div>
                            </div>
                        </div>
                        <h5 class="video-preview__descr">We frequently die in our own dreams</h5>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-4 col-sm-6">
                    <a href="./video.html" class="video-preview video-preview--md js-ajax-link">
                        <div class="video-preview__image">
                            <span class="lazy-bg-img" data-original="images/photo_14.jpg"></span>
                            <div class="video-preview__info">
                                <div class="video-preview__duration">12:04</div>
                                <div class="video-preview__likes">88%</div>
                                <div class="video-preview__quality">HD</div>
                            </div>
                        </div>
                        <h5 class="video-preview__descr">Success doesn’t come to you. You go to it</h5>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-4 col-sm-6">
                    <a href="./video.html" class="video-preview video-preview--md js-ajax-link">
                        <div class="video-preview__image">
                            <span class="lazy-bg-img" data-original="images/photo_15.jpg"></span>
                            <div class="video-preview__info">
                                <div class="video-preview__duration">12:04</div>
                                <div class="video-preview__likes">88%</div>
                                <div class="video-preview__quality">HD</div>
                            </div>
                        </div>
                        <h5 class="video-preview__descr">Every solution breeds new problems</h5>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-4 col-sm-6">
                    <a href="./video.html" class="video-preview video-preview--md js-ajax-link">
                        <div class="video-preview__image">
                            <span class="lazy-bg-img" data-original="images/photo_16.jpg"></span>
                            <div class="video-preview__info">
                                <div class="video-preview__duration">12:04</div>
                                <div class="video-preview__likes">88%</div>
                                <div class="video-preview__quality">HD</div>
                            </div>
                        </div>
                        <h5 class="video-preview__descr">Respect the past, create the future!</h5>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-4 col-sm-6">
                    <a href="./video.html" class="video-preview video-preview--md js-ajax-link">
                        <div class="video-preview__image">
                            <span class="lazy-bg-img" data-original="images/photo_17.jpg"></span>
                            <div class="video-preview__info">
                                <div class="video-preview__duration">12:04</div>
                                <div class="video-preview__likes">88%</div>
                                <div class="video-preview__quality">HD</div>
                            </div>
                        </div>
                        <h5 class="video-preview__descr">Wisdom is knowing how little we know</h5>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-4 col-sm-6">
                    <a href="./video.html" class="video-preview video-preview--md js-ajax-link">
                        <div class="video-preview__image">
                            <span class="lazy-bg-img" data-original="images/photo_18.jpg"></span>
                            <div class="video-preview__info">
                                <div class="video-preview__duration">12:04</div>
                                <div class="video-preview__likes">88%</div>
                                <div class="video-preview__quality">HD</div>
                            </div>
                        </div>
                        <h5 class="video-preview__descr">Be loyal to the one who is loyal to you</h5>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-4 col-sm-6">
                    <a href="./video.html" class="video-preview video-preview--md js-ajax-link">
                        <div class="video-preview__image">
                            <span class="lazy-bg-img" data-original="images/photo_19.jpg"></span>
                            <div class="video-preview__info">
                                <div class="video-preview__duration">12:04</div>
                                <div class="video-preview__likes">88%</div>
                                <div class="video-preview__quality">HD</div>
                            </div>
                        </div>
                        <h5 class="video-preview__descr">I am not young enough to know everything</h5>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-4 col-sm-6">
                    <a href="./video.html" class="video-preview video-preview--md js-ajax-link">
                        <div class="video-preview__image">
                            <span class="lazy-bg-img" data-original="images/photo_20.jpg"></span>
                            <div class="video-preview__info">
                                <div class="video-preview__duration">12:04</div>
                                <div class="video-preview__likes">88%</div>
                                <div class="video-preview__quality">HD</div>
                            </div>
                        </div>
                        <h5 class="video-preview__descr">Music creates the feelings which you can’t find in life</h5>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-4 col-sm-6">
                    <a href="./video.html" class="video-preview video-preview--md js-ajax-link">
                        <div class="video-preview__image">
                            <span class="lazy-bg-img" data-original="images/photo_07.jpg"></span>
                            <div class="video-preview__info">
                                <div class="video-preview__duration">12:04</div>
                                <div class="video-preview__likes">88%</div>
                                <div class="video-preview__quality">HD</div>
                            </div>
                        </div>
                        <h5 class="video-preview__descr">Fall down seven times, stand up eight</h5>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-4 col-sm-6">
                    <a href="./video.html" class="video-preview video-preview--md js-ajax-link">
                        <div class="video-preview__image">
                            <span class="lazy-bg-img" data-original="images/photo_08.jpg"></span>
                            <div class="video-preview__info">
                                <div class="video-preview__duration">12:04</div>
                                <div class="video-preview__likes">88%</div>
                                <div class="video-preview__quality">HD</div>
                            </div>
                        </div>
                        <h5 class="video-preview__descr">I remember everything what I 've forgotten</h5>
                    </a>
                </div>
                <div class="col-xl-3 col-lg-4 col-sm-6">
                    <a href="./video.html" class="video-preview video-preview--md js-ajax-link">
                        <div class="video-preview__image">
                            <span class="lazy-bg-img" data-original="images/photo_09.jpg"></span>
                            <div class="video-preview__info">
                                <div class="video-preview__duration">12:04</div>
                                <div class="video-preview__likes">88%</div>
                                <div class="video-preview__quality">HD</div>
                            </div>
                        </div>
                        <h5 class="video-preview__descr">Life is a foreign language; all men mispronounce it</h5>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

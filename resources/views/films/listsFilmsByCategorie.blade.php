@extends('layouts.frontEnd')
@section('styles')
    	<style type="text/css">
  		.scroll1{
  			overflow-y: scroll;
            height: 600px;
  		}
  	</style>
@endsection
@section('content')
    <div class="section videos-section scrollreveal scrollAnimateFade">
        <div class="container">
            <div class="section__inner">
                <div class="section-heading">
                    @if (isset($macategorie))
                        <h4>New {{ strtolower($macategorie->libelle) }} videos</h4>
                    @else
                        <h4>News videos</h4>
                    @endif
                </div>
                <div class="row">
                <div id="paginate1" class="scroll1">
                    @include('films.news')
                </div>
                </div>
              {{--<div class="text-center">{{$news->links()}}</div>--}} 
            </div>

             <div class="section__inner">
                <div class="section-heading">
                    @if (isset($macategorie))
                        <h4>Most {{ strtolower($macategorie->libelle) }} viewed</h4>
                    @else
                        <h4>Most viewed</h4>
                    @endif
                </div>
                <div class="row">
                <div id="paginate2" class="scroll2">
                    @include('films.mostview')
                </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script>
/*$(document).ready(function(){
     $('.pagination a').on('click', function (e) {
            e.preventDefault();
           var page=$(this).attr('href').split('page=')[1];
            paginate_data(page);
        });
    });
  function paginate_data(page)
   {
        $.ajax({
            url :"?page="+page,
            type : 'get',
        }).done(function (data) {
            $('#paginate1').html(data);
        }).fail(function(){
             $('#paginate1').html('Aucune donnée');
        });
     }*/
</script>
@endsection
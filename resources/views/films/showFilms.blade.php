@extends('layouts.frontEnd')
@section('styles')
    <link href="{{asset('css/video-js.min.css')}}" rel="stylesheet">
    <style>
        @media screen and (min-width: 960px) {
            .video-js .vjs-tech {
                position: absolute;
                left: 0;
                width: 100%;
                height: 100%;
            }

            .scroll {
                padding-left: 10%;
                padding-top: 15px;
                margin-top: 25px;
            }
        }

        @media screen and (max-width: 960px) {
            #video {
                height: 350px !important;
            }

            .video-js .vjs-tech {
                position: absolute;
                width: 400px !important;
                left: 0;
            }

            .vjs-poster {
                width: 400px !important;
            }

            .scroll {
                padding-left: 20%;
                padding-top: 15px;
                margin-top: 25px;
            }
        }

        .scroll {
            overflow-y: scroll;
            height: 800px;
            border: solid 1px white;
        }
    </style>
@endsection
@section('content')
    <div class="section videos-section scrollreveal scrollAnimateFade container-fluid">
        <div class="row">
            <div class="col-sm-8 container">
                <video id="video"
                       class="video-js"
                       width="960" height="auto"
                       preload="auto"
                       controls
                       poster="{{asset('uploads/images/'.$monfilm->pub_key_image) }}"
                       data-setup='{}'>
                    <source src="{{ asset('uploads/films/'.$monfilm->pub_key_film) }}"/>
                </video>
                <h5 class="video-preview__descr">{{ $monfilm->titre }}</h5>
                <div class="video-preview__quality">{{ $monfilm->nb_vues }} Vues</div>
            </div>
            <div class="col-sm-4 scroll">
                @foreach ($films as $film)
                    <a href="{{ route('voirs.show', $film->id)}}" class="video-preview js-ajax">
                        <video width="250" height="auto"
                               class="vidoe-js"
                               preload="auto"
                               poster="{{ asset('uploads/images/'.$film->pub_key_image) }}">
                            <source src="{{asset('uploads/films/'.$film->pub_key_film) }}"/>
                        </video>
                        <h6 class="video-preview__descr">{{ $film->description }}</h6>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/video.min.js')}}"></script>
    <script>
        videojs('video');
        //eviter la copie des liens de la video
        $(function () {
            $('video').bind('contextmenu', function (e) {
                return false;
            });
        });
    </script>
@endsection
@foreach ( $news as $film )
                        @if ($film->nb_vues<=5)
                            <div class="col-xl-3 col-lg-4 col-sm-6">
                                <a href="{!! route('voirs.show', $film->id) !!}"
                                   class="video-preview">
                                    <div class="video-preview__image">
                                        <span class="lazy-bg-img"
                                              data-original="{{ asset('uploads/images/'.$film->pub_key_image) }}"></span>
                                        <div class="video-preview__info">
                                            <div class="video-preview__duration">12:04</div>
                                            <div class="video-preview__likes">88%</div>
                                            <div class="video-preview__quality">{{ $film->nb_vues }} vues</div>
                                        </div>
                                    </div>
                                    <h5 class="video-preview__descr">{{ $film->description }} </h5>
                                </a>
                            </div>
                        @endif
                    @endforeach

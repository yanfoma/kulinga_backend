@extends('layouts.frontEnd')
@section('content') 
@include('partials.slider')
    <div class="container">
        <div class="row" >
                @foreach ($typeAbonnements as $type )
            <div class="col-xl-3 col-lg-4 col-sm-6">
                    <div class="panel panel-primary panel-style">
                        <div class="panel-heading">Abonnement de {{ $type->libelle}} </div>
                        <div class="panel-body">
                            <p style="color:black">
                                USD {{ number_format(($type->mt_mensuel/$type->taux_echange), 2, '.', ',')}}
                            </p>
                     </div>
                 </div>  
            </div>
        @endforeach
        </div>
        </div>
 @include('partials.footer') 
@endsection

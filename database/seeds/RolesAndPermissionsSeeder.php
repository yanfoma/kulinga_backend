<?php
/**
 * Created by PhpStorm.
 * User: Wendkouny
 * Date: 07/01/2019
 * Time: 02:53
 */

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'subscription']);
        Permission::create(['name' => 'System administration']);

        // create roles and assign created permissions

        Role::create(['name' => 'subscriber'])
            ->givePermissionTo('subscription');

        Role::create(['name' => 'Admin'])
            ->givePermissionTo(Permission::all());
    }
}
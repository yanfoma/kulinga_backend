<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrixFilmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prix_films', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('titre_prix')->nullable(false);
            $table->string('description_prix')->nullable();
            $table->unsignedDecimal('valeur_prix')->nullable();
            $table->unsignedInteger('film_id')->nullable(false);
            $table->foreign('film_id')->references('id')->on('films');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prix_films');
    }
}

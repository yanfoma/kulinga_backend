<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titre')->nullable(false)->index()->unique();
            $table->string('description')->nullable();
            $table->dateTime('date_realisation')->useCurrent();
            $table->integer('duree_film')->nullable();
            $table->bigInteger('nb_vues')->default(0);
            $table->string('url_film')->nullable(false)->unique();
            $table->string('pub_key_film')->nullable(false)->unique();
            $table->string('url_image')->nullable(false)->unique();
            $table->string('pub_key_image')->nullable(false)->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films');
    }
}

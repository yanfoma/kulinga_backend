<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreFieldToFilmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('films', function (Blueprint $table) {
            $table->string('identite_distributeur')->nullable(false);
            $table->string('adresse_distributeur')->nullable(false);
            $table->string('contact_distributeur')->nullable(false);
            $table->unsignedInteger('type_film_id')->nullable(false);
            $table->foreign('type_film_id')->references('id')->on('type_films');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('films', function (Blueprint $table) {
            $table->dropColumn(['identite_distributeur', 'adresse_distributeur', 'contact_distributeur']);
            $table->dropForeign('type_film_id');
        });
    }
}

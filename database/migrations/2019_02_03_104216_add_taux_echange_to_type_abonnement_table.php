<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTauxEchangeToTypeAbonnementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('type_abonnements', function (Blueprint $table) {
            $table->unsignedDecimal('taux_echange',8, 2)->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('type_abonnements', function (Blueprint $table) {
            $table->dropColumn('type_abonnements');
        });
    }
}

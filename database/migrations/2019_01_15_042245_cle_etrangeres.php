<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CleEtrangeres extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notations', function (Blueprint $table) {
            $table->unsignedInteger('film_id')->nullable(false);
            $table->foreign('film_id')->references('id')->on('films');
        });

        Schema::table('films', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->nullable(false);
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedInteger('auteur_id')->nullable(false);
            $table->foreign('auteur_id')->references('id')->on('auteurs');
        });

        Schema::table('abonnements', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->nullable(false);
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedInteger('typeabonnement_id')->nullable(false);
            $table->foreign('typeabonnement_id')->references('id')->on('type_abonnements');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php
use App\Http\Controllers\HomeController;
use Doctrine\DBAL\Schema\Index;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */


Route::get('/', 'HomeController@Index');

Auth::routes();

Route::get('/kulinga/admin', ['as' => 'admin.dashboard', 'uses' => 'FilmController@index']);

Route::get('admin/films', ['as' => 'admin.films.index', 'uses' => 'FilmController@index']);
Route::post('admin/films', ['as' => 'admin.films.store', 'uses' => 'FilmController@store']);
Route::get('admin/films/create', ['as' => 'admin.films.create', 'uses' => 'FilmController@create']);
Route::put('admin/films/{films}', ['as' => 'admin.films.update', 'uses' => 'FilmController@update']);
Route::patch('admin/films/{films}', ['as' => 'admin.films.update', 'uses' => 'FilmController@update']);
Route::delete('admin/films/{films}', ['as' => 'admin.films.destroy', 'uses' => 'FilmController@destroy']);
Route::get('admin/films/{films}', ['as' => 'admin.films.show', 'uses' => 'FilmController@show']);
Route::get('admin/films/{films}/edit', ['as' => 'admin.films.edit', 'uses' => 'FilmController@edit']);


Route::get('admin/prixFilms/{Films}', ['as' => 'admin.prixFilms.index', 'uses' => 'prixFilmController@index']);
Route::post('admin/prixFilms', ['as' => 'admin.prixFilms.store', 'uses' => 'prixFilmController@store']);
Route::get('admin/prixFilms/create/{Films}', ['as' => 'admin.prixFilms.create', 'uses' => 'prixFilmController@create']);
Route::put('admin/prixFilms/{prixFilms}', ['as' => 'admin.prixFilms.update', 'uses' => 'prixFilmController@update']);
Route::patch('admin/prixFilms/{prixFims}', ['as' => 'admin.prixFilms.update', 'uses' => 'prixFilmController@update']);
Route::delete('admin/prixFilms/{prixFilms}', ['as' => 'admin.prixFilms.destroy', 'uses' => 'prixFilmController@destroy']);
Route::get('admin/prixFilms/{prixFilms}', ['as' => 'admin.prixFilms.show', 'uses' => 'prixFilmController@show']);
Route::get('admin/prixFilms/{prixFilms}/edit', ['as' => 'admin.prixFilms.edit', 'uses' => 'prixFilmController@edit']);


Route::get('admin/promos', ['as' => 'admin.promos.index', 'uses' => 'PromoController@index']);
Route::post('admin/promos', ['as' => 'admin.promos.store', 'uses' => 'PromoController@store']);
Route::get('admin/promos/create', ['as' => 'admin.promos.create', 'uses' => 'PromoController@create']);
Route::put('admin/promos/{promos}', ['as' => 'admin.promos.update', 'uses' => 'PromoController@update']);
Route::patch('admin/promos/{promos}', ['as' => 'admin.promos.update', 'uses' => 'PromoController@update']);
Route::delete('admin/promos/{promos}', ['as' => 'admin.promos.destroy', 'uses' => 'PromoController@destroy']);
Route::get('admin/promos/{promos}', ['as' => 'admin.promos.show', 'uses' => 'PromoController@show']);
Route::get('admin/promos/{promos}/edit', ['as' => 'admin.promos.edit', 'uses' => 'PromoController@edit']);

Route::get('admin/auteurs', ['as' => 'admin.auteurs.index', 'uses' => 'AuteurController@index']);
Route::post('admin/auteurs', ['as' => 'admin.auteurs.store', 'uses' => 'AuteurController@store']);
Route::get('admin/auteurs/create', ['as' => 'admin.auteurs.create', 'uses' => 'AuteurController@create']);
Route::put('admin/auteurs/{auteurs}', ['as' => 'admin.auteurs.update', 'uses' => 'AuteurController@update']);
Route::patch('admin/auteurs/{auteurs}', ['as' => 'admin.auteurs.update', 'uses' => 'AuteurController@update']);
Route::delete('admin/auteurs/{auteurs}', ['as' => 'admin.auteurs.destroy', 'uses' => 'AuteurController@destroy']);
Route::get('admin/auteurs/{auteurs}', ['as' => 'admin.auteurs.show', 'uses' => 'AuteurController@show']);
Route::get('admin/auteurs/{auteurs}/edit', ['as' => 'admin.auteurs.edit', 'uses' => 'AuteurController@edit']);


Route::get('admin/abonnements', ['as' => 'admin.abonnements.index', 'uses' => 'AbonnementController@index']);
Route::post('admin/abonnements', ['as' => 'admin.abonnements.store', 'uses' => 'AbonnementController@store']);
Route::get('admin/abonnements/create', ['as' => 'admin.abonnements.create', 'uses' => 'AbonnementController@create']);
Route::put('admin/abonnements/{abonnements}', ['as' => 'admin.abonnements.update', 'uses' => 'AbonnementController@update']);
Route::patch('admin/abonnements/{abonnements}', ['as' => 'admin.abonnements.update', 'uses' => 'AbonnementController@update']);
Route::delete('admin/abonnements/{abonnements}', ['as' => 'admin.abonnements.destroy', 'uses' => 'AbonnementController@destroy']);
Route::get('admin/abonnements/{abonnements}', ['as' => 'admin.abonnements.show', 'uses' => 'AbonnementController@show']);
Route::get('admin/abonnements/{abonnements}/edit', ['as' => 'admin.abonnements.edit', 'uses' => 'AbonnementController@edit']);


Route::get('admin/typeAbonnements', ['as' => 'admin.typeAbonnements.index', 'uses' => 'TypeAbonnementController@index']);
Route::post('admin/typeAbonnements', ['as' => 'admin.typeAbonnements.store', 'uses' => 'TypeAbonnementController@store']);
Route::get('admin/typeAbonnements/create', ['as' => 'admin.typeAbonnements.create', 'uses' => 'TypeAbonnementController@create']);
Route::put('admin/typeAbonnements/{typeAbonnements}', ['as' => 'admin.typeAbonnements.update', 'uses' => 'TypeAbonnementController@update']);
Route::patch('admin/typeAbonnements/{typeAbonnements}', ['as' => 'admin.typeAbonnements.update', 'uses' => 'TypeAbonnementController@update']);
Route::delete('admin/typeAbonnements/{typeAbonnements}', ['as' => 'admin.typeAbonnements.destroy', 'uses' => 'TypeAbonnementController@destroy']);
Route::get('admin/typeAbonnements/{typeAbonnements}', ['as' => 'admin.typeAbonnements.show', 'uses' => 'TypeAbonnementController@show']);
Route::get('admin/typeAbonnements/{typeAbonnements}/edit', ['as' => 'admin.typeAbonnements.edit', 'uses' => 'TypeAbonnementController@edit']);


Route::get('admin/categories', ['as' => 'admin.categories.index', 'uses' => 'CategorieController@index']);
Route::post('admin/categories', ['as' => 'admin.categories.store', 'uses' => 'CategorieController@store']);
Route::get('admin/categories/create', ['as' => 'admin.categories.create', 'uses' => 'CategorieController@create']);
Route::put('admin/categories/{categories}', ['as' => 'admin.categories.update', 'uses' => 'CategorieController@update']);
Route::patch('admin/categories/{categories}', ['as' => 'admin.categories.update', 'uses' => 'CategorieController@update']);
Route::delete('admin/categories/{categories}', ['as' => 'admin.categories.destroy', 'uses' => 'CategorieController@destroy']);
Route::get('admin/categories/{categories}', ['as' => 'admin.categories.show', 'uses' => 'CategorieController@show']);
Route::get('admin/categories/{categories}/edit', ['as' => 'admin.categories.edit', 'uses' => 'CategorieController@edit']);

Route::get('admin/notations', ['as' => 'admin.notations.index', 'uses' => 'NotationController@index']);
Route::post('admin/notations', ['as' => 'admin.notations.store', 'uses' => 'NotationController@store']);
Route::get('admin/notations/create', ['as' => 'admin.notations.create', 'uses' => 'NotationController@create']);
Route::put('admin/notations/{notations}', ['as' => 'admin.notations.update', 'uses' => 'NotationController@update']);
Route::patch('admin/notations/{notations}', ['as' => 'admin.notations.update', 'uses' => 'NotationController@update']);
Route::delete('admin/notations/{notations}', ['as' => 'admin.notations.destroy', 'uses' => 'NotationController@destroy']);
Route::get('admin/notations/{notations}', ['as' => 'admin.notations.show', 'uses' => 'NotationController@show']);
Route::get('admin/notations/{notations}/edit', ['as' => 'admin.notations.edit', 'uses' => 'NotationController@edit']);


Route::get('admin/users', ['as' => 'admin.users.index', 'uses' => 'UsersController@index']);
Route::post('admin/users', ['as' => 'admin.users.store', 'uses' => 'UsersController@store']);
Route::get('admin/users/create', ['as' => 'admin.users.create', 'uses' => 'UsersController@create']);
Route::put('admin/users/{users}', ['as' => 'admin.users.update', 'uses' => 'UsersController@update']);
Route::patch('admin/users/{users}', ['as' => 'admin.users.update', 'uses' => 'UsersController@update']);
Route::delete('admin/users/{users}', ['as' => 'admin.users.destroy', 'uses' => 'UsersController@destroy']);
Route::get('admin/users/{users}', ['as' => 'admin.users.show', 'uses' => 'UsersController@show']);
Route::get('admin/users/{users}/edit', ['as' => 'admin.users.edit', 'uses' => 'UsersController@edit']);



Route::resource('voirs', 'RegarderFilmsController');
Route::resource('menus', 'MenuController');



Route::get('kulinga/espace_abonne', ['as' => 'espace.abonne.index', 'uses' => 'EspaceAbonneController@index']);
Route::get('kulinga/abonne', ['as' => 'espace.abonne.home', 'uses' => 'EspaceAbonneController@index']);
Route::get('kulinga/abonnement/create', ['as' => 'espace.abonne.create', 'uses' => 'EspaceAbonneController@create']);
Route::post('kulinga/abonnement/store', ['as' => 'espace.abonne.store', 'uses' => 'EspaceAbonneController@store']);
Route::post('kulinga/abonnement', ['as' => 'espace.abonne.renouveller', 'uses' => 'EspaceAbonneController@edit']);
Route::get('kulinga/abonnement/profil', ['as' => 'espace.abonne.profil', 'uses' => 'EspaceAbonneController@profile']);
Route::patch('kulinga/abonnement/update', ['as' => 'espace.abonne.update', 'uses' => 'EspaceAbonneController@update']);

Route::resource('type_film', 'TypeFilmController');
Route::resource('avis_abonne', 'AvisabonneController');